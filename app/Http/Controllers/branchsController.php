<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\carbon;

class branchsController extends Controller
{
    public $message = array();
    
    public function show_branches_PlaceID(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $place_id = $request->input('place_id');   
                
                $get_branch = \App\Branch::select('branches.id', 'place_id','address_details','city.id as city_id', 'city.name as city_name','all_area.id as area_id','all_area.name as area_name' ,'branches.Longitude','branches.Latitude','branches.main_image')
                                        ->join('city' , 'branches.city_id' ,'=' ,'city.id') 
                                        ->join('all_area' , 'branches.area_id', "=" , "all_area.id")
                                        ->where('branches.place_id', $place_id)->get();

                if( count($get_branch) >0 ){
                    $message['data'] = $get_branch;
                    $message['error'] = 0;
                    $message['message'] = "this is all the branches of this place";
                }else{
                    $message['data'] = $get_branch;
                    $message['error'] = 1;
                    $message['message'] = "this is no branches for this place";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
           
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
     public function add_branches (Request $request){
        try{
            
            $token = $request->input('token');
            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $check_token = \App\User::where('token', $token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
              
                $image = $request->file('main_image');
                
                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/places';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                     $images = "bonbel.jpeg";  
                }
                
                $add_branch = new \App\Branch;
                
                $add_branch->place_id = $request->input('place_id');
                $add_branch->main_image = $images;
                $add_branch->address_details = $request->input('address_details');
                $add_branch->city_id = $request->input('city_id');
                $add_branch->area_id = $request->input('area_id');
                $add_branch->Longitude = $request->input('Longitude');
                $add_branch->Latitude = $request->input('Latitude');
                $add_branch->created_at = $dateTime;
                $add_branch->updated_at = $dateTime;
                $add_branch->save();
                
                if( $add_branch == true ){
                    $message['error'] = 0;
                    $message['message'] = "A new branch is add successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "this is an error, please try again";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
           
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function delete_branch (Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id =  $request->input('branch_id');
                
                $delete = \App\Branch::where('id', $branch_id)->delete();
                
                if( $delete == true){
                    $message['error'] = 0;
                    $message['message'] = "this branch is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is no branch , please try again";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }            
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
      public function show_branch_ByID(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
            
              
                $branch_id = $request->input('branch_id');

                $get_branch = \App\Branch::select('branches.id', 'place_id','branches.main_image','address_details','city.id as city_id', 'city.name as city_name','all_area.id as area_id', 'all_area.name as area_name' ,'branches.Longitude','branches.Latitude')
                                        ->leftjoin('city' , 'branches.city_id' ,'=' ,'city.id') 
                                        ->leftjoin('all_area' , 'branches.area_id', "=" , "all_area.id")
                                        ->where('branches.id' , $branch_id)->first();
                

                if( $get_branch  != NULL){
                    $message['data'] = $get_branch;
                    $message['error'] = 0;
                    $message['message'] = "this is  the branches data";
                }else{
                    $message['data'] = $get_branch;
                    $message['error'] = 1;
                    $message['message'] = "this is no  data, please try again";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
           
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function update_branch  (Request $request){
        try{
          
           $token = $request->input('token');
              
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                        
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');
                
                $check_image = \App\Branch::where('id' , $branch_id)->value('main_image');
                
                 $image = $request->file('main_image');
                
                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/places';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                    if( $check_image != NULL){
                        $images = $check_image;
                    }else{
                        $images = "bonbel.jpeg";  
                    }
                }
                
                $update = \App\Branch::where('id', $branch_id)
                                     ->update([
                                        'main_image' => $images,
                                        'address_details' => $request->input('address_details'),
                                        'city_id' => $request->input('city_id'),
                                        'area_id' => $request->input('area_id'),
                                        'Longitude' => $request->input('Longitude'),
                                        'Latitude' => $request->input('Latitude'),
                                        'updated_at' => $dateTime,
                                     ]);
                
                if( $update == true){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }

    public function show_branches_AreaID(Request $request){
        try{

            $token = $request->input('token');
              
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                        
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
              
                $area_id = $request->input('area_id');

                $get_data = \App\Branch::select('branches.id','places.name as place_name','branches.main_image' ,'address_details','branches.Longitude','branches.Latitude')
                                        ->JOIN( 'places' , 'branches.place_id' ,'=', 'places.id')
                                        ->where('branches.area_id', $area_id)->get();


                if( count($get_data) >0 ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the brances in this area";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }


        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



 public function show_allbranches_or_AreaID(Request $request){
        try{

            $token = $request->input('token');
              
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                        
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
              
                $area_id = $request->input('area_id');



                if( isset($area_id)){
                    $get_data = \App\Branch::select('branches.id','places.name as place_name', 'address_details','places.logo')
                                            ->JOIN( 'places' , 'branches.place_id' ,'=', 'places.id')
                                            ->where('branches.area_id', $area_id)->get();
                        
                }else{
                     $get_data = \App\Branch::select('branches.id','places.name as place_name', 'address_details','places.logo')
                                            ->JOIN( 'places' , 'branches.place_id' ,'=', 'places.id')->get();
                    
                }

                if( count($get_data) >0 ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the brances in this area";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }


        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }

  
  
  public function show_hall_type (Request $request){
      try{
            $token = $request->input('token');
              
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                        
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
              
              $get_data = \App\Hall_type::select('id' , 'name')->get();
              
              
              if( count($get_data)>0 ){
                  $message['data'] = $get_data;
                  $message['error'] = 0;
                  $message['message'] = "this is type data";
              }else{
                  $message['data'] = $get_data;
                  $message['error'] = 1;
                  $message['message'] = "there is no data";
              }
              
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
          
      }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
  }


}
