<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Carbon\Carbon;

class userController extends Controller
{
    //
    
    public $message = array();
    
    
    public function app_login(Request $request){
        try{
            
            $email = $request->input('email');
            $password = $request->input('password');
            
            $token = hash('sha256', Str::random(60)).time().rand(100000,999999);
            
            $login = \App\User::where([['email', $email], ['password', $password],['status','3']])
                              ->orwhere([['phone', $email], ['password', $password],['status','3']])
                              ->update(['token' => $token]);  
           
            $get_data = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','status','token','created_at')
                              ->where([['email', $email], ['password', $password],['status','3']])
                              ->orwhere([['phone', $email], ['password', $password],['status','3']])->first();
                              
                    
            
            if( $login == true){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "Welcome, you are logged in";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }    
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
    
    
    
    
    public function login(Request $request){
        try{
            
            $email = $request->input('email');
            $password = $request->input('password');
            
            $token = hash('sha256', Str::random(60)).time().rand(100000,999999);
            
            $login = \App\User::where([['email', $email], ['password', $password],['status','1']])
                              ->orwhere([['phone', $email], ['password', $password],['status','1']])
                              ->update(['token' => $token]);  
           
            $get_data = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','title','status','token','created_at')
                              ->where([['email', $email], ['password', $password],['status','1']])
                              ->orwhere([['phone', $email], ['password', $password],['status','1']])->first();
                              
                    
            
            if( $login == true){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "Welcome, you are logged in";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }    
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
    
    public function logout_users(Request $request){
        try{
            $token = $request->input('token');
            
             $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $empty_token = \App\User::where('id', $check_token)->update(['token' => NULL]);
                
                if( $empty_token == true){
                    $message['error'] = 0;
                    $message['message'] = "you are logged in successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, pleaase try again";
                }
                
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
       
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
    
    public function show_user_ByID(Request $request){
        try{
            
            $token = $request->input('token');
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
            
                $user_id = $request->input('user_id');
                
                if( $request->has('user_id') == NULL || $request->has('user_id') == ''){
                    
                     $get_data = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','branch_id','place_id','status','token','created_at')
                                  ->where([['id',$check_token]])->first();
                }else{
                      $get_data = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','branch_id','place_id','status','token','created_at')
                                  ->where([['id',$user_id]])->first();

                }
              
                if( $get_data != NULL){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the data of that user";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message); 
    }
   
    
    
        public function update_adminProfile(Request $request){
        try{
            
            $token = $request->input('token');
            $image = $request->file('image');
          
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

          
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                               
                $user_id = $request->input('user_id');
                
                if( $request->has('user_id') == NULL || $request->has('user_id') == ''){
                    $id = $check_token;
                }else{
                    $id = $user_id;
                }
                
                $get_image = \App\User::where('id', $id)->value('image');

                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/users';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                    if($get_image == NULL){
                        $images = "bonbel.jpeg";
                    }else{
                     $images = $get_image;
                    }
                }
                
                $latitude = $request->input('Latitude');
                $longitude = $request->input('Longitude');
                
                if( $latitude == NULL || $latitude == '' || $longitude == NULL || $longitude == '' ){
                    $latitude = NULL;
                    $longitude = NULL;
                }

                $update_data = \App\User::where([['id', $id]])
                                         ->update([
                                            'first_name' => $request->input('first_name'),
                                            'last_name' => $request->input('last_name'),
                                            'phone' => $request->input('phone'),
                                            'email' => $request->input('email'),
                                            'password' => $request->input('password'),
                                            'image' => $images,
                                            'Latitude' => $latitude,
                                            'Longitude' => $longitude,
                                            'updated_at' => $dateTime,
                                         ]);
                
                if( $update_data == TRUE){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";        
            }
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
    
    public function show_all_users(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $get_users = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','status')
                                      ->where('status', '3')
                                      ->get();
                
                if( count($get_users) >0 ){
                    $message['data'] = $get_users;
                    $message['error'] = 0;
                    $message['message'] = "there is all the data of users";
                }else{
                    $message['data'] = $get_users;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }
                                
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }
           
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
    
    
    
      public function delete_user(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $user_id = $request->input('user_id');
                
                $delete = \App\User::where('id', $user_id)->delete();
                
                if( $delete == true ){
                    $message['error'] = 0;
                    $message['message'] = "this user is delete successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }
                                
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }
           
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
    

    public function add_bonbel_admin (Request $request){
        try{

        $token = $request->input('token');
        
        
        $check_token = \App\User::where('token',$token)->value('id');
        
        if( $request->has('token') && $check_token != NULL){

            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $phone = $request->input('phone');
            $email = $request->input('email');
            $image = $request->file('image');

            if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/users';
                $image->move($destinationPath_id, $savedFileName);

                $images = $savedFileName;
            }else{
                $images = "bonbel.jpeg";
            }


                $latitude = $request->input('Latitude');
                $longitude = $request->input('Longitude');
                
                if( $latitude == NULL || $latitude == '' || $longitude == NULL || $longitude == '' ){
                    $latitude = NULL;
                    $longitude = NULL;
                }
                
            $check_phone = \App\User::where('phone' , $phone)->value('phone');
           
           $check_email = \App\User::where('email' , $email)->value('email');

            if( $check_phone  != NULL){
                $message['error']  = 4;
                $message['message'] = "This phone is already exists, please try again";
            }elseif( $check_email != NULL ){
                $message['error']  = 5;
                $message['message'] = "This email is already exists, please try again";
            }else{
                
                $add = new \App\User;

                $add->first_name = $request->input('first_name');
                $add->last_name = $request->input('last_name');
                $add->phone = $phone;
                $add->email = $email;
                $add->password = $request->input('password');
                $add->image = $images;
                $add->Latitude = $latitude;
                $add->Longitude = $longitude;
                $add->title = $request->input('title');
                $add->place_id = NULL;
                $add->status = 1;
                $add->type	 = "accept";
                $add->created_at = $dateTime;
                $add->updated_at = $dateTime;
                $add->save();


                if( $add == true){
                    $message['error'] = 0;
                    $message['message'] = "A new admin in bonbel is add successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }

        }else{
            $message['error']= 3;
            $message['message'] = "there is no user token, please try again";
        }
       
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

        return response()->json($message);
    }

    

     
    public function show_all_bonbeAdmins(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $get_users = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','title','status')
                                      ->where('status', '1')
                                      ->get();
                
                if( count($get_users) >0 ){
                    $message['data'] = $get_users;
                    $message['error'] = 0;
                    $message['message'] = "there is all the data of bonbel admins";
                }else{
                    $message['data'] = $get_users;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }
                                
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }
           
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }

    ///  add user for resturant ::

        public function add_resturant_user(Request $request){
            try{

            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){

                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $phone = $request->input('phone');
                $email = $request->input('email');

                $image = $request->file('image');

                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/users';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                    $images = "bonbel.jpeg";
                }
                 
                $latitude = $request->input('Latitude');
                $longitude = $request->input('Longitude');
                
                if( $latitude == NULL || $latitude == '' || $longitude == NULL || $longitude == '' ){
                    $latitude = NULL;
                    $longitude = NULL;
                }
                
                

                $check_phone = \App\User::where('phone' , $phone)->value('phone');
                     
               $check_email = \App\User::where('email' , $email)->value('email');

                if( $check_phone  != NULL){
                    $message['error']  = 4;
                    $message['message'] = "This phone is already exists, please try again";
                }elseif( $check_email != NULL){
                    $message['error']  = 5;
                    $message['message'] = "This email is already exists, please try again";
                }else{
                    
                    $add = new \App\User;

                    $add->first_name = $request->input('first_name');
                    $add->last_name = $request->input('last_name');
                    $add->phone = $phone;
                    $add->email = $email;
                    $add->password = $request->input('password');
                    $add->image = $images;
                    $add->Latitude = $latitude;
                    $add->Longitude = $longitude;
                    $add->title = $request->input('title');
                    $add->place_id = $request->input('place_id');
                    $add->status = 2;
                    $add->type	 = "accept";
                    $add->created_at = $dateTime;
                    $add->updated_at = $dateTime;
                    $add->save();


                    if( $add == true){
                        $message['error'] = 0;
                        $message['message'] = "A new user resturant is add successfully";
                    }else{
                        $message['error'] = 1;
                        $message['message'] = "there is an error, please try again";
                    }
                }

            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }
           
            }catch(Exception $ex){
                $message['error']=2;
                $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }

            return response()->json($message);
        }
    



        public function show_Resturant_usersAccept(Request $request){
            try{

                $token = $request->input('token');
            
            
                $check_token = \App\User::where('token',$token)->value('id');
                
                if( $request->has('token') && $check_token != NULL){

                    $place_id = $request->input('place_id');

                    $get_data = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','title','branch_id','place_id','status','type','created_at')
                                         ->where([['place_id' , $place_id], ['status' , '2'],['type' , "accept"]])->get();   

                    

                    if( count($get_data) >0 ){
                        $message['data'] = $get_data;
                        $message['error'] = 0;
                        $message['message'] = "this is all the accepted users in that resturant";
                    }else{
                        $message['data'] = $get_data;
                        $message['error'] = 1;
                        $message['message'] = "There is no users in that resturant";
                    } 

                }else{
                    $message['error'] = 3;
                    $message['message'] = "there is no user token, please try again";
                }    

            }catch(Exception $ex){
                $message['error']=2;
                $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }

            return response()->json($message);
        }


        
        public function show_Resturant_usersWaitting(Request $request){
            try{

                $token = $request->input('token');
            
            
                $check_token = \App\User::where('token',$token)->value('id');
                
                if( $request->has('token') && $check_token != NULL){

                    $place_id = $request->input('place_id');

                    $get_data = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','title','branch_id','place_id','status','type','created_at')
                                         ->where([['place_id' , $place_id], ['status' , '2'],['type' , "wait"]])->get();   

                    

                    if( count($get_data) >0 ){
                        $message['data'] = $get_data;
                        $message['error'] = 0;
                        $message['message'] = "this is all the waitting users in that resturant";
                    }else{
                        $message['data'] = $get_data;
                        $message['error'] = 1;
                        $message['message'] = "There is no users in that resturant";
                    } 

                }else{
                    $message['error'] = 3;
                    $message['message'] = "there is no user token, please try again";
                }    

            }catch(Exception $ex){
                $message['error']=2;
                $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }

            return response()->json($message);
        }

        

        // add resturant admin from resturant dash  and must accepted from  main dash::

        public function add_resturant_admin(Request $request){
            try{

                $token = $request->input('token');
                
                
                $check_token = \App\User::where('token',$token)->value('id');
                
                if( $request->has('token') && $check_token != NULL){

                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $phone = $request->input('phone');
                $email = $request->input('email');
                $image = $request->file('image');

                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/users';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                    $images = "bonbel.jpeg";
                }
                
                $latitude = $request->input('Latitude');
                $longitude = $request->input('Longitude');
                
                if( $latitude == NULL || $latitude == '' || $longitude == NULL || $longitude == '' ){
                    $latitude = NULL;
                    $longitude = NULL;
                }

                $check_phone = \App\User::where('phone' , $phone)->value('phone');
               
               $check_email = \App\User::where('email' , $email)->value('email');

                if( $check_phone  != NULL){
                    $message['error']  = 4;
                    $message['message'] = "This phone is already exists, please try again";
                }elseif( $check_email != NULL ){
                    $message['error']  = 5;
                    $message['message'] = "This email is already exists, please try again";
                }else{
                    
                    $add = new \App\User;

                    $add->first_name = $request->input('first_name');
                    $add->last_name = $request->input('last_name');
                    $add->phone = $phone;
                    $add->email = $email;
                    $add->password = $request->input('password');
                    $add->image = $images;
                    $add->Latitude = $latitude;
                    $add->Longitude = $longitude;
                    $add->title = $request->input('title');
                    $add->place_id = $request->input('place_id');
                    $add->status = 2;
                    $add->type	 = "wait";
                    $add->created_at = $dateTime;
                    $add->updated_at = $dateTime;
                    $add->save();


                    if( $add == true){
                        $message['error'] = 0;
                        $message['message'] = "A new user resturant is add successfully";
                    }else{
                        $message['error'] = 1;
                        $message['message'] = "there is an error, please try again";
                    }
                }

            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }
           
            }catch(Exception $ex){
                $message['error']=2;
                $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }

            return response()->json($message);
        }


        public function accept_users(Request $request){
            try{
                    
                $token = $request->input('token');
                
                
                $check_token = \App\User::where('token',$token)->value('id');
                
                if( $request->has('token') && $check_token != NULL){

                    $user_id = $request->input('user_id');

                    $accepted = \App\User::where('id' , $user_id)->update(["type" => "accept"]);

                    if( $accepted  == true){
                        $message['error'] = 0;
                        $message['message'] = "this user is accepted successfully";
                    }else{
                        $message['error'] = 1;
                        $message['message'] = "there is an error, please try again";
                    }

                }else{
                    $message['error'] = 3;
                    $message['message'] = "there is no user token, please try again";
                }
                
            }catch(Exception $ex){
                $message['error']=2;
                $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }

            return response()->json($message);
        }
    



        // login resturant dashboard ::
        
        public function login_resturant_dash(Request $request){
            try{

                    $email = $request->input('email');
                    $password = $request->input('password');
                    $token = hash('sha256', Str::random(60)).time().rand(100000,999999);


                    $check_login = \App\User::where([["email" , $email], ["password" , $password] , ["status" , "2"] , ["type" , "accept"]])
                                            ->orwhere([["phone" , $email], ["password" , $password] , ["status" , "2"] , ["type" , "accept"]])
                                            ->update(["token" => $token]);

                      
                    $get_data = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','title','branch_id','place_id','status','token','created_at')
                                        ->where([["email" , $email], ["password" , $password] , ["status" , "2"] , ["type" , "accept"]])
                                        ->orwhere([["phone" , $email], ["password" , $password] , ["status" , "2"] , ["type" , "accept"]])
                                        ->first();
                   
                    if( $check_login == true){
                        $message['data'] = $get_data;
                        $message['error'] = 0;
                        $message['message']  = "Welcome you are loggied in Resturant Dashboard";
                    }else{
                        $message['data'] = $get_data;
                        $message['error'] = 1;
                        $message['message'] = "there is an error, please try again";
                    }


            }catch(Exception $ex){
                $message['error']=2;
                $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }

            return response()->json($message);
        }



        public function block_user(Request $request){
            try{
                
                 $token = $request->input('token');
                
                
                $check_token = \App\User::where('token',$token)->value('id');
                
                if( $request->has('token') && $check_token != NULL){
                    
                    
                    $created_at = carbon::now()->toDateTimeString();
                    $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                    $user_id = $request->input('user_id');
                    
                    $get_user = \App\User::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','confirm_code','title','branch_id' ,'place_id','status','type','firebase_token','token','created_at','updated_at')
                                         ->where('id' , $user_id)->first();      
                                         
                    
                    
                    $block = new \App\Block_user;
                    
                    $block->first_name = $get_user->first_name;
                    $block->last_name = $get_user->last_name;
                    $block->phone = $get_user->phone;
                    $block->email = $get_user->email;
                    $block->password = $get_user->password;
                    $block->image = $get_user->image;
                    $block->Latitude = $get_user->Latitude;
                    $block->Longitude = $get_user->Longitude;
                    $block->confirm_code = $get_user->confirm_code;
                    $block->title = $get_user->title;
                    $block->branch_id = $get_user->branch_id;
                    $block->place_id = $get_user->place_id;
                    $block->status = $get_user->status;
                    $block->type = $get_user->type;
                    $block->firebase_token = $get_user->firebase_token;
                    $block->token = NULL;
                    $block->created_at = $dateTime;
                    $block->updated_at = $dateTime;
                    $block->save();
                    
                    
                    if( $block == true){
                        
                        $deleteUser = \App\User::where('id' , $user_id)->delete();
                        
                        if( $deleteUser == true ){
                            $message['error'] = 0;
                            $message['message'] = "this user is blocked successfully";
                        }else{
                            $message['error'] = 1;
                            $message['message'] = "there is an error, please try again";
                        }
                        
                    }else{
                            $message['error'] = 1;
                            $message['message'] = "there is an error, please try again";
                        }
                    
                    
                }else{
                    $message['error'] = 3;
                    $message['message'] = "there is no user token, please try again";
                }
                
                
            }catch(Exception $ex){
                $message['error']=2;
                $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }

            return response()->json($message);
        }


        public function unblock_user(Request $request){
            try{
                
                 $token = $request->input('token');
                
                
                $check_token = \App\User::where('token',$token)->value('id');
                
                if( $request->has('token') && $check_token != NULL){
                    
                    $created_at = carbon::now()->toDateTimeString();
                    $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                    $user_id = $request->input('user_id');
                    
                    $get_userBlock = \App\Block_user::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','confirm_code','title','branch_id' ,'place_id','status','type','firebase_token','token','created_at','updated_at')
                                         ->where('id' , $user_id)->first();   
                    
                    
                    
                      $unblock = new \App\User;
                    
                    $unblock->first_name = $get_userBlock->first_name;
                    $unblock->last_name = $get_userBlock->last_name;
                    $unblock->phone = $get_userBlock->phone;
                    $unblock->email = $get_userBlock->email;
                    $unblock->password = $get_userBlock->password;
                    $unblock->image = $get_userBlock->image;
                    $unblock->Latitude = $get_userBlock->Latitude;
                    $unblock->Longitude = $get_userBlock->Longitude;
                    $unblock->confirm_code = $get_userBlock->confirm_code;
                    $unblock->title = $get_userBlock->title;
                    $unblock->branch_id = $get_userBlock->branch_id;
                    $unblock->place_id = $get_userBlock->place_id;
                    $unblock->status = $get_userBlock->status;
                    $unblock->type = $get_userBlock->type;
                    $unblock->firebase_token = $get_userBlock->firebase_token;
                    $unblock->token = NULL;
                    $unblock->created_at = $dateTime;
                    $unblock->updated_at = $dateTime;
                    $unblock->save();
                    
                    
                    if( $unblock == true){
                        
                        $deleteUser = \App\Block_user::where('id' , $user_id)->delete();
                        
                        if( $deleteUser == true ){
                            $message['error'] = 0;
                            $message['message'] = "this user is unblocked successfully";
                        }else{
                            $message['error'] = 1;
                            $message['message'] = "there is an error, please try again";
                        }
                        
                    }else{
                            $message['error'] = 1;
                            $message['message'] = "there is an error, please try again";
                        }
                    
                }else{
                    $message['error'] = 3;
                    $message['message'] = "there is no user token, please try again";
                }
              
                
                
            }catch(Exception $ex){
                $message['error']=2;
                $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }

            return response()->json($message);
        }
        
        
        
         public function show_users_block(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $get_users = \App\Block_user::select('id','first_name','last_name','phone','email','password','image','Latitude','Longitude','title','status')
                                     ->get();
                
                if( count($get_users) >0 ){
                    $message['data'] = $get_users;
                    $message['error'] = 0;
                    $message['message'] = "there is all the data of blocked users";
                }else{
                    $message['data'] = $get_users;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }
                                
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }
           
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
        
    
}
