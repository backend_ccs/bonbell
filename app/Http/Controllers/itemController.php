<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;

class itemController extends Controller
{
    public $message = array();


    public function show_items_menuCatId(Request $request){
        try{
            
            $data = array();
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
            
                $menuCat_id = $request->input('menuCat_id');

                $get_data = DB::select("SELECT id,name,price,image,details, rate , discount,state, created_at FROM `items`WHERE menuCat_id=? order by case when sort_num is null then 1 else 0 end, sort_num",[$menuCat_id]);
                
                
                
                 
                
                foreach($get_data as $item){

                    array_push($data , (object)array(
                        
                        "id" => $item->id,
                        "name" => $item->name,
                        "price" => $item->price,
                        "image" => $item->image,
                        "details" => $item->details,
                        "rate" => $item->rate,
                        "discount" => $item->discount,
                        "state" => $item->state,
                        "created_at" => $item->created_at,
                        ));
                }
                
                //\App\Item::select('id','name','price','image','details','rate','discount','state','created_at')
                  //                  ->where('menuCat_id' , $menuCat_id)->get();


                $get_name = \App\Menu_category::where('id' , $menuCat_id )->value('name');
                
                if( count($data) >0 ){
                    $message['data'] = $data;
                    $message['menu_cat'] = $get_name;
                    $message['error'] = 0;
                    $message['message'] = "This is all the items of that category";
                }else{
                    $message['data'] = $data;     
                    $message['menu_cat'] = $get_name;
                    $message['error'] = 1;
                    $message['message'] = "there is no item in that category";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }


    public function add_items(Request $request){
        try{

            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
            
                  
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $image = $request->file('image');

                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/products';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                    $images = "bonbel.jpeg";
                }

                $add = new \App\Item;

                $add->name = $request->input('name');
                $add->price = $request->input('price');
                $add->image = $images;
                $add->details = $request->input('details');
                $add->rate = 0;
                $add->branch_id = $request->input('branch_id');
                $add->menuCat_id = $request->input('menuCat_id');
                $add->state = "on";
                $add->created_at = $dateTime;
                $add->updated_at = $dateTime;
                $add->save();


                if( $add == true){
                    $message['error']  = 0;
                    $message['message'] = "This product is add successfuly";
                }else{
                    $message['error'] = 1;
                    $message['message'] ="there is an error, please try again ";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }


    public function item_avaliable(Request $request){
        try{


            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
          
                $item_id = $request->input('item_id');

                $check = \App\Item::where('id' , $item_id)->value('state');

                if($check == "on"){
                    $active = "off";
                }else{
                    $active = "on";
                }


                $update = \App\Item::where('id' , $item_id)->update([ "state" => $active]);

                if( $update == true){
                    $message['error'] = 0;
                    $message['message'] = "this item is now ".$active;
                }else{
                    $message['error']  = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please  try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    
    public function delete_item(Request $request){
        try{


            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
          
                $item_id = $request->input('item_id');

                $delete = \App\Item::where('id' , $item_id)->delete();

                if( $delete == true){
                    $message['error'] = 0;
                    $message['message'] = "this item is deleted successfully";
                }else{
                    $message['error']  = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please  try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }


    
    public function show_items_ById(Request $request){
        try{
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
            
                $item_id = $request->input('item_id');

                $get_data = \App\Item::select('id','name','price','image','details','discount','created_at')
                                    ->where('id' , $item_id)->first();


                if( count($get_data) >0 ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "This is the item data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no item, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }


    
    
    public function update_item(Request $request){
        try{


            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
          
                  
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $item_id = $request->input('item_id');

                $check_image = \App\Item::where('id', $item_id)->value('image');



                $image = $request->file('image');

                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/products';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                    if( $check_image  != NULL ){
                        $images = $check_image;
                    }else{
                        $images = "bonbel.jpeg";
                    }
                }

                $update = \App\Item::where('id' , $item_id)
                                    ->update([

                                        "name" => $request->input('name'),
                                        "price" => $request->input('price'),
                                        "image" => $images,
                                        "details" => $request->input('details'),
                                        "discount" => $request->input('discount'),
                                        "updated_at" => $dateTime
                                    ]);

                if( $update == true){
                    $message['error'] = 0;
                    $message['message'] = "this item is updated successfully";
                }else{
                    $message['error']  = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please  try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    public function show_items_branchID(Request $request){
        try{
            $token = $request->input('token');
            
            $data = array();
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
            

                $branch_id = $request->input('branch_id');
                
                
                $get_data = DB::select("SELECT id,name,price,image,details, rate , discount,state, created_at FROM `items`WHERE state='on' and branch_id=? order by case when sort_num is null then 1 else 0 end, sort_num",[$branch_id]);

                /*$get_data = \App\Item::select('id','name','price','image','details','rate','discount','state','created_at')
                                    ->where('branch_id' ,$branch_id)->get();*/


                foreach($get_data as $item){

                    array_push($data , (object)array(
                        
                        'id' => $item->id,
                        "name" => $item->name,
                        "price" => (string)$item->price,
                        "image" => $item->image,
                        "details" => $item->details,
                        "rate" => (string)$item->rate,
                        "discount" => (string)$item->discount,
                        "state" => $item->state,
                        "created_at" => $item->created_at,
                        ));
                }
                if( count($data) >0 ){
                    $message['data'] = $data;
                    $message['error'] = 0;
                    $message['message'] = "This is all the items of that category";
                }else{
                    $message['data'] = $data;
                    $message['error'] = 1;
                    $message['message'] = "there is no item in that category";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    // update sorting ::
    
    public function update_sorting_item(Request $request){
        try{
             
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
            
            
                $item_id = $request->input('item_id');
                $sort_num = $request->input('sort_num');
                
                $check = \App\Item::where('sort_num' , $sort_num)->value('id');
                
                if( $check != NULL){
                    $message['error'] = 1;
                    $message['message'] = "this sorting number is already exist";
                    
                }else{
                    
                    $update = \App\Item::where('id' , $item_id)->update(['sort_num' => $sort_num]);
                    
                    if( $update == true ){
                        $message['error'] = 0;
                        $message['message'] = "this sorting is updated successfully";
                    }else{
                        $message['error'] = 1;
                        $message['message'] = "there is an error, please try again";
                    }
                }
                 
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function  show_recommended(Request $request){
        try{
             
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
            
                $all = array();
                $item_id = $request->input('item_id');
                
                $data = \App\Item::select('branch_id', 'menuCat_id')->where('id' , $item_id)->first();
                
                $get_data = \App\Item::select('id','name','price','image','details', 'rate' , 'discount','state', 'created_at as date')
                                        ->where([['branch_id' , $data->branch_id] , ['menuCat_id' , $data->menuCat_id] , ['rate' , '>=' , '3']])->limit(6)->get();
                if( count($get_data) == 0 ){
                   
                   $get_data = \App\Item::select('id','name','price','image','details', 'rate' , 'discount','state', 'created_at as date')
                                        ->where([['branch_id' , $data->branch_id] , ['menuCat_id' , $data->menuCat_id] ])->orderBy('created_at' , 'DESC')->limit(6)->get();
                }                        
                foreach($get_data as $item){

                    array_push($all , (object)array(
                        
                        'id' => $item->id,
                        "name" => $item->name,
                        "price" => (string)$item->price,
                        "image" => $item->image,
                        "details" => $item->details,
                        "rate" => (string)$item->rate,
                        "discount" => (string)$item->discount,
                        "state" => $item->state,
                        "created_at" => (string)$item->date,
                        ));
                }
                
                if( count($get_data)>0 ){
                    $message['data'] = $all;
                    $message['error'] = 0;
                    $message['message'] = " recomended items";
                }else{
                    $message['data'] = $all;
                    $message['error'] = 1;
                    $message['message'] = "no recommended";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
               
    }
    
}
