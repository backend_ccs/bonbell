<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class branchDetailsController extends Controller
{
    public $message = array();


    
    // branch phones ::

    public function show_phone_branchId(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $branch_id = $request->input('branch_id');

                $get_data = \App\Branch_phone::select('id', 'phone', 'email','created_at')
                                      ->where('branch_id' , $branch_id)->get();

                if( count($get_data )>0){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the phones for that branch";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no phone, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    public function add_branch_phone(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){    
                
               $created_at = carbon::now()->toDateTimeString();
               $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

               $add = new \App\Branch_phone;

               $add->phone = $request->input('phone');
               $add->email = $request->input('email');
               $add->branch_id = $request->input('branch_id');
               $add->created_at = $dateTime;
               $add->updated_at = $dateTime;
               $add->save();
               
               if( $add == true){
                   $message['error'] = 0;
                   $message['message'] = "A new phone is add successfully";
               }else{
                   $message['error'] = 1;
                   $message['message'] = "there is an error, please try again";
               }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }      
 
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    

    public function show_phone_ById(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $phone_id = $request->input('phone_id');

                $get_data = \App\Branch_phone::select('id', 'phone','email', 'created_at')
                                     ->where('id', $phone_id)->first();

                if( $get_data != NULL ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the phone data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    

    public function update_phone_branch(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $phone_id = $request->input('phone_id');
                
                $update_data = \App\Branch_phone::where('id', $phone_id)
                                    ->update([
                                       "phone" => $request->input('phone'),
                                       "email" => $request->input('email'),
                                       "updated_at" => $dateTime,
                                    ]);

                if( $update_data == true){
                    $message['error'] = 0;
                    $message['message'] = "phone data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }


    
    public function delete_phone(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){ 
                
                $phone_id = $request->input('phone_id');
                
                $delete = \App\Branch_phone::where('id', $phone_id)->delete();
                
                if( $delete == true){
                    $message['error'] = 0;
                    $message['message'] = " this phone is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }         

        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    //types of all branches ::Order_type



    public function show_order_types(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          


                $get_data = \App\Order_type::select('id', 'name', 'created_at')->get();

                if( count($get_data )>0){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the types for branches";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }


    
    // branch types of orders ::

    public function show_type_branchId(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $branch_id = $request->input('branch_id');

                $get_data = \App\Branch_type::select('branch_types.id','order_types.id as orderType_id','order_types.name', 'branch_types.created_at')
                                           ->join("order_types" ,"branch_types.orderType_id" , "=", "order_types.id")                                 
                                           ->where('branch_types.branch_id' , $branch_id)->get();

                if( count($get_data )>0){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the types for that branch";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no phone, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    public function add_branch_type(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){    
                
               $created_at = carbon::now()->toDateTimeString();
               $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
                
               $branch_id =  $request->input('branch_id');
               $orderType_id = $request->input('orderType_id');
               
               $check = \App\Branch_type::where([['branch_id' , $branch_id] , ['orderType_id' , $orderType_id]])->first();
               
               if( $check != NULL){
                   $message['error'] = 1;
                   $message['message'] = "this branch already has this type";
               }else{ 
                   $add = new \App\Branch_type;
    
                   $add->branch_id = $branch_id;
                   $add->orderType_id = $orderType_id;
                   $add->created_at = $dateTime;
                   $add->updated_at = $dateTime;
                   $add->save();
                   
                   
                   if( $add == true){
                       $message['error'] = 0;
                       $message['message'] = "A new branch type is add successfully";
                   }else{
                       $message['error'] = 1;
                       $message['message'] = "there is an error, please try again";
                   }
               } 
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }      
 
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function delete_branch_type(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){ 
                
                $type_id = $request->input('type_id');
                
                $delete = \App\Branch_type::where('id', $type_id)->delete();
                
                if( $delete == true){
                    $message['error'] = 0;
                    $message['message'] = " this phone is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }         

        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
}
