<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\carbon;
use Illuminate\Support\Facades\DB;

class menuTypeController extends Controller
{
    
       
    public $message = array();
    
    
    public function show_allmenu_page(Request $request){
        try{
             $token = $request->input('token');
            
            $data = array();
            $all_menu = array();
            $cat_items = array();

            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');    
                    
                    $branch_name = \App\Branch::select('places.name')->join('places' , 'branches.place_id' ,'=' ,'places.id')
                                                ->where('branches.id' , $branch_id)->first();
                   
                    $all_menu[] = (object)array(
                         "id" => -1,
                         "name" =>"All Menu",
                         "time_from" => "00:00:45",
                         "time_to" => "Any Time",
                         "branch_id" => $branch_id,
                         "branch_name" => $branch_name->name,
                         "created_at" => "2020-05-07 03:47:57",
                         "updated_at" => "2020-05-07 03:47:57",
                         "category" => [],
                         "items" => []
                        );
                        
            
                $get_data = \App\Menu_types::select('menu_types.id', 'menu_types.name','time_from','time_to','branches.id as branch_id' ,'places.name as branch_name','menu_types.created_at as date','menu_types.updated_at as datex')
                                            ->leftjoin('branches' , 'menu_types.branch_id', '=', 'branches.id')
                                            ->leftjoin('places' , 'branches.place_id' ,'=' ,'places.id')
                                            ->where([['menu_types.branch_id', $branch_id]])->get();
                
                
                                                    
                foreach($get_data as $type){
                     $catsx = array();
            
                    $items_type = DB::select("SELECT items.id, items.name,price, items.image,details, rate , discount,state, items.created_at FROM `items`
                                                left JOIN menu_category on items.menuCat_id = menu_category.id
                                                left JOIN menu_types on menu_category.menuType_id = menu_types.id
                                                WHERE menu_types.id=? order by case when sort_num is null then 1 else 0 end, sort_num",[$type->id]);

                    $menu_cat = \App\Menu_category::select('menu_category.id', 'menu_category.name','menu_category.image','menu_types.id as menuType_id','menu_types.name as menuType_name' ,'menu_category.created_at as date','menu_category.updated_at as datss')
                                                ->leftjoin('menu_types'  , 'menu_category.menuType_id' ,'=' ,'menu_types.id')
                                                ->where([['menu_category.menuType_id', $type->id]])->get();
                    
                     foreach($menu_cat as $cat){
                        $item_typecat = array();

                        $items_cat = DB::select("SELECT id,name,price,image,details, rate , discount,state, created_at FROM `items`WHERE menuCat_id=? order by case when sort_num is null then 1 else 0 end, sort_num",[$cat->id]);
                
                        foreach($items_cat as $item){
        
                            array_push($item_typecat , (object)array(
                                
                                "id" => $item->id,
                                "name" => $item->name,
                                "price" => $item->price,
                                "image" => $item->image,
                                "details" => $item->details,
                                "rate" => $item->rate,
                                "discount" => $item->discount,
                                "state" => $item->state,
                                "created_at" => $item->created_at,
                                ));
                        }
                        
                        array_push($catsx , (object)array(
                            "id" => $cat->id,
                            "name" => $cat->name,
                            "image" => $cat->image,
                            "menuType_id" => $cat->menuType_id,
                            "menuType_name" => $cat->menuType_name,
                            "created_at" => $cat->date,
                            "updated_at" => $cat->datss,
                            "items" => $item_typecat
                            ));
                        
                        
                     }
                                
                    
                    
                   
                    
                    

                    array_push($all_menu , (object)array(
                        "id" => $type->id,
                        "name" => $type->name,
                        "time_from" => $type->time_from,
                        "time_to" => $type->time_to,
                        "branch_id" => $type->branch_id,
                        "branch_name" => $type->branch_name,
                        "created_at" => $type->date,
                        "updated_at" => $type->datex,
                        "category" => $catsx,
                        "items" => $items_type
                        ));
                }
              
                
                $items = DB::select("SELECT id,name,price,image,details, rate , discount,state, created_at FROM `items`WHERE state='on' and branch_id=? order by case when sort_num is null then 1 else 0 end, sort_num",[$branch_id]);

                foreach($items as $item){

                    array_push($data , (object)array(
                        
                        'id' => $item->id,
                        "name" => $item->name,
                        "price" => (string)$item->price,
                        "image" => $item->image,
                        "details" => $item->details,
                        "rate" => (string)$item->rate,
                        "discount" => (string)$item->discount,
                        "state" => $item->state,
                        "created_at" => $item->created_at,
                        ));
                }
                
                 $category = \App\Menu_category::select('menu_category.id', 'menu_category.name','menu_category.image' ,'menu_category.created_at as date','menu_category.updated_at as datss')
                                                ->where([['menu_category.branch_id' , $branch_id]])->get();
                                                
                    foreach( $category as $caats){
                         $itemcat = array();

                        $items_cat = DB::select("SELECT id,name,price,image,details, rate , discount,state, created_at FROM `items`WHERE menuCat_id=? order by case when sort_num is null then 1 else 0 end, sort_num",[$caats->id]);
                
                        foreach($items_cat as $item){
        
                            array_push($itemcat , (object)array(
                                
                                "id" => $item->id,
                                "name" => $item->name,
                                "price" => $item->price,
                                "image" => $item->image,
                                "details" => $item->details,
                                "rate" => $item->rate,
                                "discount" => $item->discount,
                                "state" => $item->state,
                                "created_at" => $item->created_at,
                                ));
                        }
                                
                         array_push($cat_items , (object)array(
                            "id" => $caats->id,
                            "name" => $caats->name,
                            "image" => $caats->image,
                            "menuType_id" => $caats->menuType_id,
                            "menuType_name" => $caats->menuType_name,
                            "created_at" => $caats->date,
                            "updated_at" => $caats->datss,
                            "items" => $itemcat
                            ));
                    }                                
               
                    
                $all = [
                    
                    "menu" => $all_menu,
                    "category" => $cat_items,
                    "items" => $data,
                    ];
            
            
                if($all != NULL){
                    $message['data'] = $all;
                    $message['error'] = 0;
                    $message['message'] = "all branch data";
                }else{
                    $message['data'] = $all;
                    $message['error'] = 1;
                    $message['message'] = "No data for that branch";
                }
            
            }
            } catch(Exception $ex){
              $message['error']=2;
              $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }
          return response()->json($message);
        }
    
    
    
    
    public function show_menuType_branchID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');    

                
                $get_data = \App\Menu_types::select('id', 'name','time_from','time_to', 'created_at','updated_at')
                                     ->where([['branch_id', $branch_id]])->get();
                
                 $get_id = \App\Branch::where('id' , $branch_id)->value('place_id');
                
                 $branchname = \App\Place::where('id' , $get_id)->value('name');
                
                if( count($get_data )>0  ){
                    $message['data'] = $get_data;
                    $message['branch_name'] = $branchname;
                    $message['error'] = 0;
                    $message['message'] = "this is all the menu types of that place";
                }else{
                    $message['data'] = $get_data;
                    $message['branch_name'] = $branchname;
                    $message['error'] = 1;
                    $message['message'] = "there is no menus for that place";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
     public function add_menuType_branchID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                               
                $insert = new \App\Menu_types;
                
                $insert->name = $request->input('name');
                $insert->time_from = $request->input('time_from');
                $insert->time_to = $request->input('time_to');                
                $insert->branch_id = $request->input('branch_id');
                $insert->created_at = $dateTime;
                $insert->updated_at = $dateTime;
                $insert->save();
                
                if( $insert == true  ){
                    $message['error'] = 0;
                    $message['message'] = "a new menu type is add to that place successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function delete_menuType(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $menu_id = $request->input('menu_id');
                
                $delete = \App\Menu_types::where('id', $menu_id)->delete();
                
                if( $delete == true  ){

                    $get_menucat = \App\Menu_category::select('id')->where('menuType_id', $menu_id)->get();
                    $delete_menu = \App\Menu_category::where('menuType_id', $menu_id)->delete();

                    foreach( $get_menucat as $ids){
                        $delete_items = \App\Item::where('menuCat_id' , $ids->id)->delete();
                    }
                    $message['error'] = 0;
                    $message['message'] = "this menu is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
      public function show_menuType_ById(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $menu_id = $request->input('menu_id');
                
                $get_data = \App\Menu_types::select('id', 'name', 'time_from','time_to','created_at','updated_at')
                                     ->where('id', $menu_id)->first();
                
                if( $get_data != NULL  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the menu data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no menu for that place";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function update_menuType(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $menu_id = $request->input('menu_id');
                
                $update = \App\Menu_types::where('id' , $menu_id)
                                     ->update([
                                        "name" => $request->input('name'),
                                        "time_from" => $request->input('time_from'),
                                        "time_to" => $request->input('time_to'), 
                                        "updated_at" => $dateTime,
                                    ]);
              
                
                if( $update == true  ){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    

}
