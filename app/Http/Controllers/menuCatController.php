<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class menuCatController extends Controller
{
        
    public $message = array();
    
    
     public function show_menuCat_branchID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');    

                
                $get_data = \App\Menu_category::select('id', 'name','image', 'created_at','updated_at')
                                     ->where([['branch_id', $branch_id]])->get();
                
                if( count($get_data )>0  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the menu category of that branch";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no category in this type";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function show_menuCat_typeID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $menuType_id = $request->input('menuType_id');    

                
                $get_data = \App\Menu_category::select('id', 'name','image', 'created_at','updated_at')
                                     ->where([['menuType_id', $menuType_id]])->get();
                
                $get_name = \App\Menu_types::where('id' , $menuType_id)->value('name');
                
                if( count($get_data )>0  ){
                    $message['data'] = $get_data;
                    $message['menu_type'] = $get_name; 
                    $message['error'] = 0;
                    $message['message'] = "this is all the menu category of that place";
                }else{
                    $message['data'] = $get_data;  
                    $message['menu_type'] = $get_name; 
                    $message['error'] = 1;
                    $message['message'] = "there is no category in this type";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
     public function add_menuCat_typeID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $image = $request->file('image');
                
                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/menuCategory';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                     $images = "bonbel.jpeg";  
                }

                               
                $insert = new \App\Menu_category;          
                
                $insert->name = $request->input('name');
                $insert->image = $images;
                $insert->branch_id = $request->input('branch_id');
                $insert->menuType_id = $request->input('menuType_id');
                $insert->created_at = $dateTime;
                $insert->updated_at = $dateTime;
                $insert->save();
                
                if( $insert == true  ){
                    $message['error'] = 0;
                    $message['message'] = "a new category is add to that menu type successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function delete_menuCat(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $menucat_id = $request->input('menucat_id');    
                
                $delete = \App\Menu_category::where('id', $menucat_id)->delete();
                
                if( $delete == true  ){

                    $delete_items = \App\Item::where('menuCat_id' , $menuCat_id)->delete();

                    $message['error'] = 0;
                    $message['message'] = "this category is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
      public function show_menuCat_ById(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $menucat_id = $request->input('menucat_id');    
                
                $get_data = \App\Menu_category::select('id', 'name', 'image','created_at','updated_at')
                                     ->where('id', $menucat_id)->first();
                
                if( $get_data != NULL  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the category data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no category for that type";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function update_menuCat(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $menucat_id = $request->input('menucat_id');    
                
                
                $get_image = \App\Menu_category::where('id',$menucat_id)->value('image');
                
                 $image = $request->file('image');
                
                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/menuCategory';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                    if( $get_image != NULL){
                        $images = $get_image;
                    }else{
                        $images = "bonbel.jpeg";
                    }
                }
                $update = \App\Menu_category::where('id' , $menucat_id)
                                     ->update([
                                        "name" => $request->input('name'),
                                        "image" => $images,
                                        "updated_at" => $dateTime,
                                    ]);
              
                
                if( $update == true  ){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    

}
