<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\carbon;

class sizesController extends Controller
{
    
    public $message = array();
    
    public function show_sizes_itemID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');
                $item_id = $request->input('item_id');

                
                $get_data = \App\Sizes::select('id', 'name','price', 'created_at','updated_at')
                                     ->where([['branch_id', $branch_id],['item_id', $item_id]])->get();
                
                if( count($get_data )>0  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the sizes of that item";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no sizes for that place";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
     public function add_sizes_itemID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                               
                $insert = new \App\Sizes;
                
                $insert->name = $request->input('name');
                $insert->price = $request->input('price');
                $insert->item_id = $request->input('item_id');
                $insert->branch_id = $request->input('branch_id');
                $insert->created_at = $dateTime;
                $insert->updated_at = $dateTime;
                $insert->save();
                
                if( $insert == true  ){
                    $message['error'] = 0;
                    $message['message'] = "a new size is add to that item successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function delete_sizes(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $size_id = $request->input('size_id');
                
                $delete = \App\Sizes::where('id', $size_id)->delete();
                
                if( $delete == true  ){
                    $message['error'] = 0;
                    $message['message'] = "this size is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
      public function show_size_ById(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $size_id = $request->input('size_id');
                
                $get_data = \App\Sizes::select('id', 'name', 'price','created_at','updated_at')
                                     ->where('id', $size_id)->first();
                
                if( $get_data != NULL  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the size data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no size for that place";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function update_sizes(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $size_id = $request->input('size_id');
                
                $update = \App\Sizes::where('id' , $size_id)
                                     ->update([
                                        "name" => $request->input('name'),
                                        "price" => $request->input('price'),
                                        "updated_at" => $dateTime,
                                    ]);
              
                
                if( $update == true  ){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    


}
