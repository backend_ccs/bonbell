<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\carbon;


class sauceController extends Controller
{
    public $message = array();
    
    public function show_sauce_branchID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');
                
                $get_data = \App\Sauce::select('id', 'name', 'created_at')
                                     ->where('branch_id', $branch_id)->get();
                
                if( count($get_data )>0  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the sauces of this place";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no sauces for that place";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
     public function add_sauce_branchID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $branch_id = $request->input('branch_id');
                
                $insert = new \App\Sauce;
                $insert->name = $request->input('name');
                $insert->branch_id = $branch_id;
                $insert->created_at = $dateTime;
                $insert->updated_at = $dateTime;
                $insert->save();
                
                if( $insert == true  ){
                    $message['error'] = 0;
                    $message['message'] = "a new sauce is add successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function delete_sauce(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $sauce_id = $request->input('sauce_id');
                
                $delete = \App\Sauce::where('id', $sauce_id)->delete();
                
                if( $delete == true  ){
                    $message['error'] = 0;
                    $message['message'] = "this Sauce is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
      public function show_sauce_ById(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $sauce_id = $request->input('sauce_id');
                
                $get_data = \App\Sauce::select('id', 'name', 'created_at')
                                     ->where('id', $sauce_id)->first();
                
                if( $get_data != NULL  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the sauce data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no sauces for that place";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function update_sauce(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $sauce_id = $request->input('sauce_id');
                
                $update =  \App\Sauce::where('id' , $sauce_id)
                                     ->update([
                                        "name" => $request->input('name'),
                                        "updated_at" => $dateTime,
                                    ]);
              
                
                if( $update == true  ){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
}
