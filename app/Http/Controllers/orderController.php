<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\carbon;
use Illuminate\Support\Facades\DB;

class orderController extends Controller
{
    public $message = array();
    
    
    public function make_order(Request $request){
        try{
            
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
             
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            
                $branch_id = $request->input('branch_id');
                $data = $request->json()->all();
    
                     $add_bill = new \App\Bill;
                    
                    $add_bill->user_id = $check_token;
                    $add_bill->total_price = $request->input('total_price');
                    $add_bill->comment = $request->input('comment');
                    $add_bill->branch_id = $branch_id;
                    $add_bill->created_at = $dateTime;
                    $add_bill->save();
                    
                    foreach( $data['orderDetails'] as $dd){
                         
                         $add = new \App\Order;
                        
                        $add->item_id = $dd['item_id'];
                        $add->size_id = $dd['size_id'];
                        $add->sauce_id = $dd['sauce_id'];
                        $add->softDrink_id = $dd['softDrink_id'];
                        $add->price = $dd['price'];
                        $add->quantity = $dd['quantity'];
                        $add->notes = $dd['notes'];
                        $add->user_id = $check_token;
                        $add->status = "need";
                        $add->table_num = $dd['table_num'];
                        $add->branch_id = $branch_id;
                        $add->bill_id = $add_bill->id;
                        $add->type = $dd['type'];
                        $add->splitting = "all";
                        $add->created_at = $dateTime;
                        $add->updated_at = $dateTime;
                        $add->save();
                    }
               
              
                
                
              
                
               
                if( $add == true){
                    $message['bill_id'] = $add_bill->id;
                    $message['error'] = 0;
                    $message['message'] = "there is an new item is add in the order";
                }else{        
                    $message['bill_id'] = $add_bill->id;
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
    

            
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    public function show_orderNeed_userID(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $order_data = \App\Order::select('orders.id as order_id', 'orders.item_id','items.name as item_name', 'orders.quantity', 'orders.price')
                                        ->join('items' , 'orders.item_id', '=', 'items.id')
                                        ->where([['orders.status' , 'need'] , ['orders.user_id' , $check_token]])->get();
             
             
                $get_taxis = \App\aboutUs::select('tax', 'service')
                                         ->where('id', '1')->first();
                
                
                if( count( $order_data) > 0){
                    $message['data'] = $order_data;
                    $message['taxs'] = $get_taxis;
                    $message['error'] = 0;
                    $message['message'] = "this is all the items order of that user";
                }else{
                    $message['data'] = $order_data;
                    $message['taxs'] = $get_taxis;
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
             
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function confirm_order(Request $request){
        try{
            
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                

                $add_bill = new \App\Bill;
                
                $add_bill->user_id = $check_token;
                $add_bill->total_price = $request->input('total_price');
                $add_bill->comment = $request->input('comment');
                $add_bill->branch_id = $request->input('branch_id');
                $add_bill->created_at = $dateTime;
                $add_bill->save();
                
                 $update_order = \App\Order::where([['user_id' , $check_token],['status', 'need']])
                                         ->update([
                                             
                                             "status" => "request",
                                             "bill_id" => $add_bill->id,
                                             "updated_at" => $dateTime,
                                             
                                             ]);   
                
                if( $add_bill == true){
                    $message['bill_id'] = $add_bill->id;
                    $message['error'] = 0;
                    $message['message'] = "this order is confirmed successfully";
                }else{
                    $message['bill_id'] = $add_bill->id;
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
             
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function cancel_item(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $order_id = $request->input('order_id');
                
                $delete_item = \App\Order::where([['user_id' , $check_token] , ['id' , $order_id]])->delete();
                
                
                if( $delete_item == true ){
                    $message['error'] = 0;
                    $message['message'] = "this order item is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error. please try again"; 
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
           
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function show_order_time(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $bill_id = $request->input('bill_id');
                
                $show_time = \App\Order::where([['bill_id', $bill_id], ['status' , 'accept']])->value('preparing_time');
                
                if( $show_time != NULL){
                    $message['data']  = $show_time;
                    $message['error'] = 0;
                    $message['message'] = "this is the preparing time of that order";
                }else{
                    $message['data'] = $show_time;
                    $message['error'] = 1;
                    $message['message'] = "this order isn't accepted yet";
                }
             
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function show_bill_details(Request $request){
        try{
            
             $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $bill_id = $request->input('bill_id');
                $branch_id = $request->input('branch_id');
                
               
                   
                         
                        $get_data = \App\Order::select('orders.id as order_id', 'orders.item_id', 'items.name as item_name', 'orders.quantity', 'orders.price')
                                            ->join('items' , 'orders.item_id' , '=', 'items.id')
                                            ->where([ ['orders.branch_id' , $branch_id] , ['orders.user_id' , $check_token] , ['orders.bill_id' , $bill_id] , ['orders.status' , 'accept'], ['splitting' , 'all']])->get();
                            
                          
                                    
                if( count($get_data) >0 ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the details of that bill";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no bill data";
                    
                }
          
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
     
      
    
    
    
    public function split_item(Request $request){
        try{
            
             $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

               
                $split_data = array();
                
                $order_id = $request->input('order_id');
                $bill_id = $request->input('bill_id');
                $user_id = $request->input('user_id');
                
                $get_orderPrice = \App\Order::where('id' , $order_id)->value('price');
                
                $users = explode(',', $user_id);
                
                $price_split = $get_orderPrice / count($users);
                
                foreach( explode(',', $user_id)  as $id){
                    
                 
                    
                    array_push( $split_data , array(
                        
                        "bill_id"  => $bill_id,
                        "order_id" => $order_id,
                        "user_id"  => $id,
                        "price"    => $price_split,
                        "created_at" => $dateTime,
                        ));
                    
                }             
            
                $add_spliting = \App\Splitting::insert($split_data);
                
                $update_order = \App\Order::where('id', $order_id)->update(['splitting' => 'split']);
                
                if( $add_spliting == true  &&  $update_order == true){
                    $message['error'] = 0;
                    $message['message'] = "this order item is splitting successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    
    public function show_billSpliting_details(Request $request){
        try{
            
             $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $bill_id = $request->input('bill_id');
                
                $get_order_split = \App\Order::where('bill_id' , $bill_id)->value('splitting');
                
                $all = array();
                  
                $get_taxis = \App\aboutUs::select('tax', 'service')
                                         ->where('id', '1')->first();


                    if( $get_order_split == 'split'){
                        
                        $check_user = \App\Splitting::select('users.id as user_id', 'users.first_name', 'users.last_name')
                                                    ->JOIN('users' , 'splitting_order.user_id', '=' ,'users.id')
                                                    ->where('splitting_order.bill_id' , $bill_id)->distinct()->get();


                        foreach($check_user as $user){    
                                
                                $total = 0;
                                $tax = 0;
                                $service = 0;
                                $round_tax=0;
                                $round_service=0;
                                
                            $get_data = \App\Splitting::select('splitting_order.id as split_id', 'splitting_order.order_id', 'orders.item_id' , 'items.name as item_name', 'orders.quantity', 'splitting_order.price'
                                                               )
                                                    ->JOIN('orders' , 'splitting_order.order_id', '=',  'orders.id')
                                                    ->JOIN('items' , 'orders.item_id' ,'=' ,'items.id')
                                                    ->where([['splitting_order.bill_id' , $bill_id],['splitting_order.user_id' , $user->user_id]])->get();   
                                
                                
                                foreach($get_data as $money){
                                    
                                    $total += $money->price;

                                }
                                
                                $tax =  ($total * $get_taxis->tax) / 100;
                                
                                $round_tax = number_format($tax, 2);
                                 
                                $service =  ($total * $get_taxis->service) / 100;
                                
                                $round_service = number_format($service , 2);    
                                
                            array_push($all , (object)array(
                                
                                "user_id" => $user->user_id,
                                "first_name" => $user->first_name,
                                "last_name" => $user->last_name,
                                "ordersss" => $get_data,
                                "tax" => $round_tax,
                                "service" => $round_service,
                               )); 
                                                
                        }
                    }
                    
                    
                    
                    
                    if( count($all) >0 ){
                        $message['data'] = $all;
                        $message['error'] = 0;
                        $message['message'] = "this is all the splitting bill user details";
                    }else{
                        $message['data'] = $all;
                        $message['error'] = 1;
                        $message['message'] = "there  bill isn't split yet";
                    }
                                                
                                                
                 
          
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }         
    
    
    
    public function pay_bill(Request $request){
        try{
            
             $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

               
                $bill_id = $request->input('bill_id');
                $pay_method = $request->input('pay_method');
                
                $update = \App\Order::where('bill_id' , $bill_id)->update(['status' => 'payed', 'updated_at' => $dateTime]);
                
                $update_method = \App\Bill::where('id' , $bill_id)->update(['pay_method' => $pay_method , 'updated_at' => $dateTime]);
                
                if( $update == true &&  $update_method == true ){
                    $message['error'] = 0;
                    $message['message'] = "this order is payed successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                
                
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    // confirm order fast food ::
    
     public function confirm_order_fast_pick(Request $request){
        try{
            
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                

                $add_bill = new \App\Bill;
                
                $add_bill->user_id = $check_token;
                $add_bill->total_price = $request->input('total_price');
                $add_bill->comment = $request->input('comment');
                $add_bill->branch_id = $request->input('branch_id');
                $add_bill->created_at = $dateTime;
                $add_bill->save();
                
                 $update_order = \App\Order::where([['user_id' , $check_token],['status', 'need']])
                                         ->update([
                                             
                                             "status" => "request",
                                             "bill_id" => $add_bill->id,
                                             "updated_at" => $dateTime,
                                             
                                             ]);   
                
                if( $add_bill == true){
                    $message['bill_id'] = $add_bill->id;
                    $message['error'] = 0;
                    $message['message'] = "this order is confirmed successfully";
                }else{
                    $message['bill_id'] = $add_bill->id;
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
             
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
     
    public function show_billDetails_fast_pick(Request $request){
        try{
            
             $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $bill_id = $request->input('bill_id');
                $branch_id = $request->input('branch_id');
                
               
                   
                         
                    $get_data = \App\Order::select('orders.id as order_id', 'orders.item_id', 'items.name as item_name', 'orders.quantity', 'orders.price' , 'bills.total_price')
                                        ->join('items' , 'orders.item_id' , '=', 'items.id')
                                        ->join('bills' , 'orders.bill_id' , '=', 'bills.id')
                                        ->where([ ['orders.branch_id' , $branch_id] , ['orders.user_id' , $check_token] , ['orders.bill_id' , $bill_id] , ['orders.status' , 'accept'], ['splitting' , 'all']])->get();
                        
                    $total_price = \App\Bill::where('id' , $bill_id)->value('total_price');    
                                    
                if( count($get_data) >0 ){
                    $message['data'] = $get_data;
                    $message['total'] = $total_price;
                    $message['error'] = 0;
                    $message['message'] = "this is all the details of that bill";
                }else{
                    $message['data'] = $get_data;
                    $message['total'] = $total_price;
                    $message['error'] = 1;
                    $message['message'] = "there is no bill data";
                    
                }
          
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    
    
      
    public function add_reservation(Request $request){
        try{
            
             $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                
                $add = new \App\Reservation;
                
                $add->user_id = $check_token;
                $add->hall_type = $request->input('hall_type');
                $add->num_people = $request->input('num_people');
                $add->date = $request->input('date');
                $add->time = $request->input('time');
                $add->branch_id = $request->input('branch_id');
                $add->deposit = NULL;
                $add->pay_method = NULL;
                $add->status = "need";
                $add->created_at = $dateTime;
                $add->updated_at = $dateTime;
                $add->save();

                
                if( $add == true ){
                    $message['error'] = 0;
                    $message['message'] = "A new reservation is add successfuly";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                    
                }
          
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function show_reservation_confirm(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $get_data = \App\Reservation::select('id',  DB::raw('DAYNAME(date) as date_name'), 'date', 'time' )
                                        ->where([['user_id' , $check_token ] , ['status' , 'accept']])
                                        ->orderBy('created_at' , 'DESC')->first();
                                        
                if( $get_data != NULL){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the last reservation confirmed";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no reservation yet";
                }
            
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function pay_reservation_deposit(Request $request){
        try{
            
             $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

               
                $reservation_id = $request->input('reservation_id');
                $deposit = $request->input('deposit');
                $pay_method = $request->input('pay_method');
                
                $check = \App\Reservation::where( 'id' , $reservation_id)->value('status');
                
                if( $check == 'accept'){
                    
                      $update = \App\Reservation::where('id' , $reservation_id)->update([ 
                                        'deposit' => $deposit ,
                                        "pay_method" => $pay_method,
                                        'status' => 'payed', 
                                        'updated_at' => $dateTime]);
                    
                    $message['error'] = 0;
                    $message['message'] = "this reservation is payed successfully";    
                                    
                

                }elseif($check == 'payed'){
                    
                    $message['error'] = 1;
                    $message['message'] = "this reservation is already payed ";    
                    
                }else{
                    
                    $message['error'] = 1;
                    $message['message'] = "this reservation isn't accepted yet";
                }
                
                
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token , please try again";
            }
            
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function pay_deposit(Request $request){
        try{
             
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            
        
                $deposit = $request->input('deposit');
                $bill_id = $request->input('bill_id');
                $pay_method = $request->input('pay_method');

                $check_order = \App\Order::where('bill_id' ,$bill_id )->value('status');
                
                if( $check_order == 'accept' ){
                    
                    $update = \App\Order::where('bill_id' ,$bill_id )->update([
                        "deposit" => $deposit,
                        "status"  => "payed"
                        ]);
                        
                    
                    $update_method = \App\Bill::where('id' , $bill_id)->update(['pay_method' => $pay_method , 'updated_at' => $dateTime]);

                    $message['error'] = 0;
                    $message['message'] = "this order is payed successfuly";
                    
                }elseif( $check_order == 'payed'){
                    $message['error'] = 1;
                    $message['message'] = "this order is already payed";
                    
                }else{
                    $message['error'] = 1;
                    $message['message'] = "this order isn't accepted yet";
                }
                
              
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    
    //get minimum  presentage ::
    
    public function get_place_minPresent (Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');

                $get_placeID = \App\Branch::where('id' , $branch_id)->value('place_id');

                $get_data = \App\Place::where('id' , $get_placeID)->value('min_present');
                
               
                if( $get_data != NULL ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is place minimum presentage";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no minimum presentage";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
   
   
   
   // get all bills ::
   
  
     public function show_orders_billID(Request $request){
       try{
           
            $token = $request->input('token');
            
            $all = array();
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $bill_id = $request->input('bill_id');
                
                $get_data = \App\Order::select( 'orders.id', 'orders.item_id', 'items.name as item_name','items.image as item_image', 'orders.size_id' , 'sizes.name as size_name', 'orders.sauce_id', 'sauce.name as sauce_name', 'orders.softDrink_id' , 
                                                'soft_drinks.name as softdrink_name', 'orders.price','orders.quantity','orders.notes',  'orders.user_id' , 'users.first_name', 'users.last_name' , 'orders.status' , 'orders.created_at as date')
                                    ->leftJOIN( 'items' , 'orders.item_id'  , '=' , 'items.id')
                                    ->leftJOIN( 'sizes' , 'orders.size_id'  , '=' , 'sizes.id')
                                    ->leftJOIN( 'sauce' , 'orders.sauce_id' , '=' , 'sauce.id')
                                    ->leftJOIN( 'soft_drinks' , 'orders.softDrink_id' , '=' , 'soft_drinks.id')
                                    ->leftJOIN( 'users' , 'orders.user_id' , '=' , 'users.id')
                                    ->where('orders.bill_id', $bill_id)->get();
                                   
                
                foreach( $get_data  as $each){
                    
                    array_push($all , (object)array(
                        "id" => $each->id,
                        "item_id" => (string)$each->item_id,
                        "item_name" => $each->item_name,
                        "item_image" => $each->item_image,
                        "size_id" => (string)$each->size_id,
                        "size_name" => $each->size_name,
                        "sauce_id"  => (string)$each->sauce_id,
                        "sauce_name" => $each->sauce_name,
                        "softDrink_id" => (string)$each->softDrink_id,
                        "softdrink_name" => $each->softdrink_name,
                        "price" => (string)$each->price,
                        "quantity" => (string)$each->quantity,
                        "notes" => $each->notes,
                        "user_id" => (string)$each->user_id,
                        "first_name" => $each->first_name,
                        "last_name" => $each->last_name,
                        "status" => $each->status,
                        "created_at" => $each->date
                        ));
                } 
                
                
                                    
                if( count($get_data) >0 ){
                    $message['data'] = $all;
                    $message['error'] = 0;
                    $message['message'] = "this is all the orders of that bill"; 
                }else{
                    $message['data'] = $all;
                    $message['error'] = 1;
                    $message['message'] = "there is no order in that bill";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
           
       }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
   }
  
   
  
   
   // orders dashboard ::
   
   
  public function show_NeedBills_branchID(Request $request){
       try{
           
            $token = $request->input('token');
            $all = array();
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');
                $type = $request->input('type');
                
                $get_data = \App\Order::select( 'orders.bill_id', 'bills.user_id' ,'users.first_name', 'users.last_name', 'users.image', 'users.phone', 'bills.total_price', 'bills.created_at as date')
                                        ->join('bills' , 'orders.bill_id', '=', 'bills.id')
                                        ->join('users' , 'bills.user_id' ,'=' ,'users.id')
                                        ->where([['orders.status' , 'need'] , ['orders.branch_id' , $branch_id] , ['orders.type' ,$type ]])
                                        ->orderBy('bills.created_at' , 'DESC')->distinct()->get();
                
                
                foreach( $get_data as $each){
                    array_push($all , (object)array(
                        "bill_id" => (int)$each->bill_id,
                        "user_id" => (int)$each->user_id,
                        "first_name" => $each->first_name,
                        "last_name" => $each->last_name,
                        "image" => $each->image,
                        "phone" => $each->phone,
                        "total_price" => $each->total_price,
                        "created_at"  => $each->date 
                        ));
                }
                
                if( count($get_data) >0){
                    $message['data']    = $all;
                    $message['error']   = 0;
                    $message['message'] = "this is all needs bills";
                }else{
                    $message['data']    = $all;
                    $message['error']   = 1;
                    $message['message'] = "there is no need bills";
                }
                
            }else{
               $message['error'] = 3;
               $message['message'] = "there is no user token, please try again";
            }
           
           
           
       }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
   }
   
   
    public function show_AcceptBills_branchID(Request $request){
       try{
           
            $token = $request->input('token');
            $all = array();
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');
                $type = $request->input('type');
                
                $get_data = \App\Order::select( 'orders.bill_id', 'bills.user_id' ,'users.first_name', 'users.last_name', 'users.image', 'users.phone', 'bills.total_price', 'bills.created_at as date')
                                        ->join('bills' , 'orders.bill_id', '=', 'bills.id')
                                        ->join('users' , 'bills.user_id' ,'=' ,'users.id')
                                        ->where([['orders.status' , 'accept'] , ['orders.branch_id' , $branch_id] , ['orders.type' ,$type ]])
                                        ->orderBy('bills.created_at' , 'DESC')->distinct()->get();
                

 
                foreach( $get_data as $each){
                    array_push($all , (object)array(
                        "bill_id" => (int)$each->bill_id,
                        "user_id" => (int)$each->user_id,
                        "first_name" => $each->first_name,
                        "last_name" => $each->last_name,
                        "image" => $each->image,
                        "phone" => $each->phone,
                        "total_price" => $each->total_price,
                        "created_at"  => $each->date 
                        ));
                }
                
                if( count($get_data) >0){
                    $message['data'] = $all;
                    $message['error'] = 0;
                    $message['message'] = "this is all accepted bills";
                }else{
                    $message['data'] = $all;
                    $message['error'] = 1;
                    $message['message'] = "there is no accepted bills";
                }
                
            }else{
               $message['error'] = 3;
               $message['message'] = "there is no user token, please try again";
            }
           
           
           
       }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
   }        
   
    public function show_payedBills_branchID(Request $request){
       try{
           
            $token = $request->input('token');
            $all = array();
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');
                $type = $request->input('type');
                
                $get_data = \App\Order::select( 'orders.bill_id', 'bills.user_id' ,'users.first_name', 'users.last_name', 'users.image', 'users.phone', 'bills.total_price','bills.pay_method', 'bills.created_at as date')
                                        ->join('bills' , 'orders.bill_id', '=', 'bills.id')
                                        ->join('users' , 'bills.user_id' ,'=' ,'users.id')
                                        ->where([['orders.status' , 'payed'] , ['orders.branch_id' , $branch_id] , ['orders.type' ,$type ]])
                                        ->orderBy('bills.created_at' , 'DESC')->distinct()->get();
                

 
                foreach( $get_data as $each){
                    array_push($all , (object)array(
                        "bill_id" => (int)$each->bill_id,
                        "user_id" => (int)$each->user_id,
                        "first_name" => $each->first_name,
                        "last_name" => $each->last_name,
                        "image" => $each->image,
                        "phone" => $each->phone,
                        "total_price" => $each->total_price,
                        "pay_method"  => $each->pay_method,
                        "created_at"  => $each->date 
                        ));
                }
                
                if( count($get_data) >0){
                    $message['data'] = $all;
                    $message['error'] = 0;
                    $message['message'] = "this is all payed bills";
                }else{
                    $message['data'] = $all;
                    $message['error'] = 1;
                    $message['message'] = "there is no payed bills";
                }
                
            }else{
               $message['error'] = 3;
               $message['message'] = "there is no user token, please try again";
            }
           
           
           
       }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
   }        
   
   
   public function accept_orders(Request $request){
         try{
           $token = $request->input('token');

            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
              
              $bill_id = $request->input('bill_id');
              $time = $request->input('time');

              $update =  \App\Order::where('bill_id' , $bill_id)->update(['status' => 'accept' , 'preparing_time' => $time]);
              
              if( $update == true){
                  $message['error']  =0;
                  $message['message'] = "order accepted";
              }else{
                  $message['error'] = 1;
                  $message['message'] = "error, please try again";
              }
            }else{
               $message['error'] = 3;
               $message['message'] = "there is no user token, please try again";
            }
           
       }catch(Exception $ex){
              $message['error']=2;
              $message['message']="error('DataBase Error: {$ex->getMessage()}')";
            }
          return response()->json($message);
   }
   
    
    
}
?>