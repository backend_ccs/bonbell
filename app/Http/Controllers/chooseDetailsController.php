<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\carbon;

class chooseDetailsController extends Controller{
    
    public  $message = array();
    
    public function add_item_categories(Request $request){
        $token = $request->input('token');
            
        $check_token = \App\User::where('token',$token)->value('id');
                
        if( $request->has('token') && $check_token != NULL){
        
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
            $add = new \App\Choose_category;
            
            $add->item_id = $request->input('item_id');
            $add->name    = $request->input('name');
            $add->state   = $request->input('state');   //  1:: choose    2:: option
            $add->created_at = $dateTime;
            $add->save();
            
            if($add == true){
                $message['error'] = 0;
                $message['message'] = "A new category is add to this item";
            }else{
                $message['error'] = 1;
                $message['message'] = "error, please try again";
            }
            
        }else{
            $message['error'] = 3;
            $message['message'] = "there is no user token, please try again";
        }
      return response()->json($message);
    }
    
    public function show_choose_category(Request $request){
        $token = $request->input('token');
            
        $check_token = \App\User::where('token',$token)->value('id');
                
        if( $request->has('token') && $check_token != NULL){
            
            $item_id = $request->input('item_id');
            $state   = $request->input('state');
            
            $get_data = \App\Choose_category::select('id' , 'name'  )->where([['item_id' , $item_id] , ['state' , $state]])->get();
            
            if( count($get_date) >0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "all categories in this item";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] =  "no categories in this item";
            }
        }else{
            $message['error'] = 3;
            $message['message'] = "there is no user token, please try again";
        }
      return response()->json($message);
    }
    
    
     public function add_details_categories(Request $request){
        $token = $request->input('token');
            
        $check_token = \App\User::where('token',$token)->value('id');
                
        if( $request->has('token') && $check_token != NULL){
        
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
            $add = new \App\Category_details;
            
            $add->chooseCat_id = $request->input('chooseCat_id');
            $add->name    = $request->input('name');
            $add->price   = $request->input('price');   
            $add->created_at = $dateTime;
            $add->save();
            
            if($add == true){
                $message['error'] = 0;
                $message['message'] = "A new details is add to this category";
            }else{
                $message['error'] = 1;
                $message['message'] = "error, please try again";
            }
            
        }else{
            $message['error'] = 3;
            $message['message'] = "there is no user token, please try again";
        }
      return response()->json($message);
    }
    
     public function show_details_category(Request $request){
        $token = $request->input('token');
            
        $check_token = \App\User::where('token',$token)->value('id');
                
        if( $request->has('token') && $check_token != NULL){
            
            $chooseCat_id = $request->input('chooseCat_id');

            $get_data = \App\Category_details::select('id' , 'name' , 'price' , 'created_at'  )->where([['chooseCat_id' , $chooseCat_id]])->get();
            
            if( count($get_date) >0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "all details in this category";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] =  "no details in this category";
            }
        }else{
            $message['error'] = 3;
            $message['message'] = "there is no user token, please try again";
        }
      return response()->json($message);
    }
    
    
    public function show_item_page(request $request){
        $token = $request->input('token');
        $choose = array();
        $option = array();
        
        $check_token = \App\User::where('token',$token)->value('id');
                
        if( $request->has('token') && $check_token != NULL){
            
            $item_id = $request->input('item_id');
            
            $item_details = \App\Item::select('id', 'name', 'price','image','details','discount')->where([['id' ,$item_id], ['state' , 'on']] )->first();
            
            
            $choose_category = \App\Choose_category::select('id' , 'name')->where([['item_id', $item_id] , ['state' , '1']])->get();

            $option_category = \App\Choose_category::select('id' , 'name')->where([['item_id', $item_id] , ['state' , '2']])->get();
            
            foreach($choose_category as $sub){
                
                $get_details =  \App\Category_details::select('id' , 'name' , 'price' , 'created_at'  )->where([['chooseCat_id' , $sub->id]])->get();
                
                array_push($choose , (object)array(
                    "id" => $sub->id,
                    "name" => $sub->name,
                    "details" => $get_details
                    ));
            }
            
            foreach($option_category as $option){
                
                $get_details =  \App\Category_details::select('id' , 'name' , 'price' , 'created_at'  )->where([['chooseCat_id' , $option->id]])->get();
                
                array_push($data , (object)array(
                    "id" => $sub->id,
                    "name" => $sub->name,
                    "details" => $get_details
                    ));
            } 
            
            
            $all = [
                "item" =>$item_details,
                "choose" => $choose,
                "option" => $option
                ];
            
            
            if( $all != NULL){
                $message['data']=  $all;
                $message['error'] = 0;
                $message['message'] = "all item data";
            }else{
                $message['data']  = $all;
                $message['error'] = 1;
                $message['message'] = "no details";
            }
        }else{
            $message['error'] = 3;
            $message['message'] = "there is no user token, please try again";
        }
      return response()->json($message);
    }
   
    
}