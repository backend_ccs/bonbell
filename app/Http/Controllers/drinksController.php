<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class drinksController extends Controller
{
     
    public $message = array();
    
    public function show_sofyDrinks_branchID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $branch_id = $request->input('branch_id');    

                
                $get_data = \App\soft_drinks::select('id', 'name','price', 'created_at','updated_at')
                                     ->where([['branch_id', $branch_id]])->get();
                
                if( count($get_data )>0  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the soft drink of that place";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no drinks for that place";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
     public function add_softDrinks_branchID(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                               
                $insert = new \App\soft_drinks;
                
                $insert->name = $request->input('name');
                $insert->price = $request->input('price');
                $insert->branch_id = $request->input('branch_id');
                $insert->created_at = $dateTime;
                $insert->updated_at = $dateTime;
                $insert->save();
                
                if( $insert == true  ){
                    $message['error'] = 0;
                    $message['message'] = "a new drink is add to that place successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function delete_softDrink(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $drink_id = $request->input('drink_id');
                
                $delete = \App\soft_drinks::where('id', $drink_id)->delete();
                
                if( $delete == true  ){
                    $message['error'] = 0;
                    $message['message'] = "this drink is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
      public function show_softDrink_ById(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $drink_id = $request->input('drink_id');
                
                $get_data = \App\soft_drinks::select('id', 'name', 'price','created_at','updated_at')
                                     ->where('id', $drink_id)->first();
                
                if( $get_data != NULL  ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the soft drink data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no drinks for that place";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function update_softDrinks(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
 
                $drink_id = $request->input('drink_id');
                
                $update = \App\soft_drinks::where('id' , $drink_id)
                                     ->update([
                                        "name" => $request->input('name'),
                                        "price" => $request->input('price'),
                                        "updated_at" => $dateTime,
                                    ]);
              
                
                if( $update == true  ){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                        
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    


}
