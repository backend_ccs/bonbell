<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\carbon;

class cityController extends Controller
{
    public $message = array();
    
    public function show_allCity(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $get_data = \App\City::select('id', 'name', 'Longitude','Latitude')->get();

                if( count($get_data )>0){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the city data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function add_city(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){    
                
               $created_at = carbon::now()->toDateTimeString();
               $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

               $add = new \App\City;

               $add->name = $request->input('name');
               $add->Longitude = $request->input('Longitude');
               $add->Latitude = $request->input('Latitude');
               $add->created_at = $dateTime;
               $add->updated_at = $dateTime;
               $add->save();
               
               if( $add == true){
                   $message['error'] = 0;
                   $message['message'] = "A new city is add successfully";
               }else{
                   $message['error'] = 1;
                   $message['message'] = "there is an error, please try again";
               }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }      
 
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
     public function show_city_ById(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $city_id = $request->input('city_id');
                
                $get_data = \App\City::select('id', 'name', 'Longitude','Latitude')
                                     ->where('id', $city_id)->first();

                if( count($get_data )>0){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the city data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function update_city(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $city_id = $request->input('city_id');
                
                $update_data = \App\City::where('id', $city_id)
                                    ->update([
                                       "name" => $request->input('name'),
                                       "Longitude" => $request->input('Longitude'),
                                       "Latitude" => $request->input('Latitude'),
                                       "updated_at" => $dateTime,
                                    ]);

                if( $update_data == true){
                    $message['error'] = 0;
                    $message['message'] = "city data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function delete_city(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){ 
                
                $city_id = $request->input('city_id');
                
                $check_city = \App\Branch::where('city_id' , $city_id)->value('id');
                
                if($check_city != NULL ){
                    $message['error'] = 1;
                    $message['message'] = "You can't delete this city, because there is branches on it";
                }else{
                    
                    
                    $delete = \App\City::where('id', $city_id)->delete();
                    
    
                    if( $delete == true){
                        $delete_area = \App\Area::where('city_id', $city_id)->delete();
    
                        $message['error'] = 0;
                        $message['message'] = " this city is deleted successfully";
                    }else{
                        $message['error'] = 1;
                        $message['message'] = "there is an error, please try again";
                    }
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }         

        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    // country ::

    public function show_allArea_cityId(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $city_id = $request->input('city_id');

                $get_data = \App\Area::select('id', 'name', 'created_at')
                                      ->where('city_id' , $city_id)->get();

                if( count($get_data )>0){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the area in that city";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no area, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }



    public function add_area(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){    
                
               $created_at = carbon::now()->toDateTimeString();
               $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

               $add = new \App\Area;

               $add->name = $request->input('name');
               $add->city_id = $request->input('city_id');
               $add->created_at = $dateTime;
               $add->updated_at = $dateTime;
               $add->save();
               
               if( $add == true){
                   $message['error'] = 0;
                   $message['message'] = "A new area is add successfully";
               }else{
                   $message['error'] = 1;
                   $message['message'] = "there is an error, please try again";
               }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }      
 
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    

    public function show_area_ById(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $area_id = $request->input('area_id');

                $get_data = \App\Area::select('id', 'name', 'created_at')
                                     ->where('id', $area_id)->first();

                if( $get_data != NULL ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the area data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    

    public function update_Area(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){          

                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $area_id = $request->input('area_id');
                
                $update_data = \App\Area::where('id', $area_id)
                                    ->update([
                                       "name" => $request->input('name'),
                                       "updated_at" => $dateTime,
                                    ]);

                if( $update_data == true){
                    $message['error'] = 0;
                    $message['message'] = "Area data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }


    
    public function delete_area(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){ 
                
                $area_id = $request->input('area_id');
                
                $check_area = \App\Branch::where('area_id' , $area_id)->value('id');

                if( $check_area != NULL){
                    $message['error'] = 1;
                    $message['message'] = "You can't delete this area, because there is branches on it";
                }else{
                    
                    $delete = \App\Area::where('id', $area_id)->delete();
                    
                    if( $delete == true){
                        $message['error'] = 0;
                        $message['message'] = " this area is deleted successfully";
                    }else{
                        $message['error'] = 1;
                        $message['message'] = "there is an error, please try again";
                    }
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }         

        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    public function show_city_area(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){ 
                
                $data = array();
                
                $get_city=  \App\City::select('id as city_id','name as city_name')->get();
                
                foreach( $get_city  as $city){
                    $get_area  = \App\Area::select('id as area_id', 'name as area_name')->where('city_id' , $city->city_id )->get();
                
                    array_push( $data,  (object)array(
                        
                        "city_id" => $city->city_id,
                        "city_name" => $city->city_name,
                        "area" => $get_area,
                        
                        ));    
                }


                if( count($data) >0 ){
                    $message['data'] = $data;
                    $message['error'] = 0;
                    $message['message'] = " this is the city and it's area";
                }else{
                    $message['data'] = $data;
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }         

        } catch(Exception $ex){ 
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
}
