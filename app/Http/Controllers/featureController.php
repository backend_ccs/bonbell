<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class featureController extends Controller
{
    public $messge = array();


    public function show_bonbel_features(Request $request){
        try{

            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
           
                $get_data = \App\Bonbel_feature::select('id', 'details','created_at')->get();

                if( count($get_data) >0 ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the features of bonbel admins";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no bonbel features";
                }
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }



    
    public function show_vendor_features(Request $request){
        try{
            
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
           
                $get_data = \App\vendor_feature::select('id', 'details','created_at')->get();

                if( count($get_data) >0 ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the features of vendors admins";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no vendor features";
                }
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }

    public function  add_user_feature(Request $request){
        try{

            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){

                $created_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
    
                $user_id = $request->input('user_id');
                $feature_id = $request->input('feature_id');

                $check = \App\User_feature::where([['user_id' , $user_id],['feature_id' , $feature_id]])->value('id');
                if( $check != NULL){
                    $message['error'] = 1;
                    $message['message'] = "this user has the same feature, so please try again";
                }else{

                    $add = new \App\User_feature;

                    $add->user_id = $user_id;
                    $add->feature_id = $feature_id;
                    $add->type = $request->input('type');
                    $add->created_at = $dateTime;
                    $add->updated_at = $dateTime;
                    $add->save();
    
                    if( $add == true){
                        $message['error'] = 0;
                        $message['message'] = "A new feature is add to that user";
                    }else{
                        $message['error'] = 1;
                        $message['message'] = "there is an errorm please try again";
                    }
               
                }
               
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }

        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }


    public function show_features_userID(Request $request){
        try{

            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){

                $get_data = array();
                $user_id = $request->input('user_id');

                $check_type = \App\User_feature::where('user_id', $user_id)->value('type');

                if( $check_type == "bonbel"){

                    $get_data = \App\User_feature::select('user_features.id','user_features.feature_id','bonbel_features.details', 'user_features.created_at')
                                                ->join('bonbel_features' , 'user_features.feature_id' ,'=' ,'bonbel_features.id')
                                                ->where('user_id' , $user_id)->get();
                }elseif( $check_type == "vendor"){
                    
                     $get_data = \App\User_feature::select('user_features.id','user_features.feature_id','vendor_features.details', 'user_features.created_at')
                                            ->join('vendor_features' , 'user_features.feature_id' ,'=' ,'vendor_features.id')
                                            ->where('user_id' , $user_id)->get();
                }

                if( count($get_data) > 0){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all features of that user";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = " there is no fetures for that user";
                }
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }


        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }


    public function delete_userFeature(Request $request){
        try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){

                $userfeature_id = $request->input('userfeature_id');

                $delete = \App\User_feature::where('id' , $userfeature_id)->delete();

                if( $delete == true){
                    $message['error'] = 0;
                    $message['message'] = "this user feature is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }


        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
    
    
    public function show_code_data(Request $request){
         try{
            
            $token = $request->input('token');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                //type 0  table 00 branch 000

                $code = $request->input('code');
                
                $message['type'] = substr($code, 0,1);
                $message['table'] = substr($code, 1,2);
                $message['branch'] = substr($code, 3,5);

                $branch = substr($code, 3,5);
                if(substr($branch, 0,1) == 0){
                    if(substr($branch, 1,1) == 0){
                        $branch_id = substr($branch, 2,3) ;
                    }else{
                        $branch_id = substr($branch, 1,3) ; 
                    }
                }else{
                     $branch_id = substr($branch, 0,3) ; 
                }

                $get_branchname = \App\Branch::select('places.name')->join('places' , 'places.id', '=' ,'branches.place_id')->where('branches.id' ,$branch_id )->first();
                
                $message['branch_name'] = $get_branchname->name;
                
                
            }else{
                $message['error']= 3;
                $message['message'] = "there is no user token, please try again";
            }


        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }

      return response()->json($message);
    }
}
