<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\carbon;

class aboutController extends Controller
{
    public function show_aboutUs(Request $request){
        try{
            $token = $request->input('token');
        
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){
               
                $get_data = \App\aboutUs::select('name', 'phone','address','email','tax','service','updated_at')
                                    ->where('id', '1')->first();

                if( $get_data != NULL){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = " this is all the data of about us";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
       public function update_aboutUs(Request $request){
        try{
            $token = $request->input('token');
        
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){
               
               $created_at = carbon::now()->toDateTimeString();
               $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $get_data = \App\aboutUs::where('id', '1')
                                        ->update([
                                            "name" => $request->input('name'),
                                            "phone" => $request->input('phone'),
                                            "address" => $request->input('address'),
                                            "email" => $request->input('email'),
                                            "tax" => $request->input('tax'),
                                            "service" => $request->input('service'),
                                            "updated_at" => $dateTime
                                        ]);

                if( $get_data == true){
                    $message['error'] = 0;
                    $message['message'] = " the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
   
   
    public function show_termsCondition(Request $request){
        try{
            $token = $request->input('token');
        
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){
               
                $get_data = \App\Terms_condition::select('id', 'title','content','updated_at')
                                              ->where('id', '1')->first();

                if( $get_data != NULL){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = " this is all the data of Terms and condition";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no data, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    } 
    
    
    
     public function update_termsCondition(Request $request){
        try{
            $token = $request->input('token');
        
            $check_token = \App\User::where('token', $token)->value('id');

            if( $request->has('token') &&  $check_token != NULL){
               
               $created_at = carbon::now()->toDateTimeString();
               $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                $get_data = \App\Terms_condition::where('id', '1')
                                        ->update([
                                            "title" => $request->input('title'),
                                            "content" => $request->input('content'),
                                            "updated_at" => $dateTime
                                        ]);

                if( $get_data == true){
                    $message['error'] = 0;
                    $message['message'] = " the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
            
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
}
