<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class placeController extends Controller
{
    //
    
    public $message = array();
    
     
    // places : ************************************
    
    
    public function show_places(Request $request){
        try{
            $token = $request->input('token');
            
            $check_token = \App\User::where('token', $token)->value('id');
            
            if($request->has('token') && $check_token != NULL){
                
                $get_allplaces = \App\Place::select('id', 'name','email','rate','image','logo','created_at')->get();
                
                if(count($get_allplaces)>0){
                    $message['data'] = $get_allplaces;
                    $message['error'] = 0;
                    $message['message'] = "this is all the places data";
                }else{
                    $message['data'] = $get_allplaces;
                    $message['error'] = 1;
                    $message['message'] = "there is no places, please trya again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function show_place_ByID(Request $request){
        try{
            $token = $request->input('token');
            
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                
                $place_id = $request->input('place_id');
                
                $get_data = \App\Place::select('id', 'name','email','rate','image','logo','min_present','created_at')
                                       ->where('id',$place_id)->first();
                
                if( $get_data != NULL){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the data ";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = " there is an error, please try again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] ="there is no user token, please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function add_place(Request $request){
        try{
            
            $token = $request->input('token');
           
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

           
            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
               
                
                $image = $request->file('image');
                $logo = $request->file('logo');

                if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/places';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                     $images = "bonbel.jpeg";  
                }

                if(isset($logo)){
                    $new_name = $logo->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/places';
                    $logo->move($destinationPath_id, $savedFileName);

                    $logos = $savedFileName;
                }else{
                     $logos = "bonbel.jpeg";
                }
                
                $addplace = new \App\Place;
                
                $addplace->name = $request->input('name');
                $addplace->email = $request->input('email');
                $addplace->rate = 0;
                $addplace->image = $images;
                $addplace->logo = $logos;
                $addplace->min_present = $request->input('min_present');
                $addplace->created_at = $dateTime;
                $addplace->updated_at = $dateTime;
                $addplace->save();
                
                $add_branch = new \App\Branch;
                
                $add_branch->place_id = $addplace->id;
                $add_branch->address_details = $request->input('address_details');
                $add_branch->area_id = $request->input('area_id');
                $add_branch->city_id = $request->input('city_id');
                $add_branch->Longitude = $request->input('Longitude');
                $add_branch->Latitude = $request->input('Latitude');
                $add_branch->created_at = $dateTime;
                $add_branch->updated_at = $dateTime;
                $add_branch->save();
                
                if($addplace == TRUE && $add_branch == true){
                    $message['error'] = 0;
                    $message['message'] = "A new place with data of branch is add successfully";
                }else{
                    $message['error'] = 1;
                    $message['error'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']= "error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function update_place(Request $request){
        try{
            $token = $request->input('token');
            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $check_token = \App\User::where('token', $token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $place_id = $request->input('place_id');

                $image = $request->file('image');
                $logo = $request->file('logo');

                $get_images = \App\Place::select('image','logo')->where('id',$place_id)->first();
                
                 if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/places';
                    $image->move($destinationPath_id, $savedFileName);

                    $images = $savedFileName;
                }else{
                    if($get_images->image == NULL){
                        $images = "bonbel.jpeg";
                    }else{
                        $images = $get_images->image;  
                    }
                }

                if(isset($logo)){
                    $new_name = $logo->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/places';
                    $logo->move($destinationPath_id, $savedFileName);

                    $logos = $savedFileName;
                }else{
                    if($get_images->logo == NULL){
                        $logos = "bonbel.jpeg";
                    }else{
                        $logos = $get_images->logo;  
                    }
                }
                
                
                
                $update_place = \App\Place::where('id', $place_id)
                                                  ->update([
                                                    'name' => $request->input('name'),
                                                    'email' => $request->input('email'),
                                                    'image' => $images,
                                                    'logo' => $logos,
                                                    'min_present' => $request->input('min_present'),
                                                    'updated_at' => $dateTime,
                                                  ]);  
                
                if( $update_place == true){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please trya again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    // this must delte all the following:
    
    public function delete_places(Request $request){
        try{
            $token  = $request->input('token');

            $check_token = \App\User::where('token', $token)->value('id');
            
            if($request->has('token') && $check_token != NULL){
                
                $place_id = $request->input('place_id');

                $delete_branch = \App\Branch::where('place_id', $place_id)->delete();
                
                $delete = \App\Place::where('id', $place_id)->delete();
                
                if( $delete ==  true){
                    $message['error'] = 0;
                    $message['message'] = "this place is deleted successfully";
                }else{
                    $message['error']  =1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    // place description : ************************************
    
    
    public function show_placeDescription(Request $request){
        try{
            $token = $request->input('token');
            $place_id =$request->input('place_id');
            
            $check_token = \App\User::where('token', $token)->value('id');
            
            if($request->has('token') && $check_token != NULL){
                
                $get_alldescription = \App\place_description::select('id','title','description','place_id','created_at')
                                        ->where('place_id' , $place_id)->get();
                
                if(count($get_alldescription)>0){
                    $message['data'] = $get_alldescription;
                    $message['error'] = 0;
                    $message['message'] = "this is all the main category";
                }else{
                    $message['data'] = $get_alldescription;
                    $message['error'] = 1;
                    $message['message'] = "there is no main category, please trya again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function show_description_ByID(Request $request){
        try{
            $token = $request->input('token');
            
            $description_id = $request->input('description_id');
            
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
                $get_data = \App\place_description::select('id','title','description','place_id','created_at')
                                             ->where('id',$description_id)->first();
                
                if( $get_data != NULL){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the data";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = " there is an error, please try again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] ="there is no user token, please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function add_description(Request $request){
        try{
            
            $token = $request->input('token');
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $check_token = \App\User::where('token',$token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $adddescription = new \App\place_description;
                
                $adddescription->title = $request->input('title');
                $adddescription->description = $request->input('description');
                $adddescription->place_id = $request->input('place_id');
                $adddescription->created_at = $dateTime;
                $adddescription->save();
                
                if($adddescription == TRUE){
                    $message['error'] = 0;
                    $message['message'] = "A new description is add successfully";
                }else{
                    $message['error'] = 1;
                    $message['error'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function update_description(Request $request){
        try{
            $token = $request->input('token');
            $description_id = $request->input('description_id');
            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $check_token = \App\User::where('token', $token)->value('id');
            
            if( $request->has('token') && $check_token != NULL){
                
                $update_description = \App\place_description::where('id', $description_id)
                                                  ->update([
                                                     "title" => $request->input('title'),
                                                      "description" => $request->input('description'),
                                                      "updated_at" => $dateTime,
                                                  ]);  
                
                if( $update_description == true){
                    $message['error'] = 0;
                    $message['message'] = "the data is updated successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please trya again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    public function delete_description(Request $request){
        try{
            $token  = $request->input('token');
            $description_id = $request->input('description_id');
            $check_token = \App\User::where('token', $token)->value('id');
            
            if($request->has('token') && $check_token != NULL){
                
                $delete = \App\place_description::where('id', $description_id)->delete();
                
                if( $delete ==  true){
                    $message['error'] = 0;
                    $message['message'] = "this place description is deleted successfully";
                }else{
                    $message['error']  =1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    
    // place imsgaes ::
    
    public function show_branch_images(Request $request){
        try{
            $token = $request->input('token');
            $branch_id =$request->input('branch_id');
            
            $check_token = \App\User::where('token', $token)->value('id');
            
            if($request->has('token') && $check_token != NULL){
                
                $get_place_images = \App\Branch_images::select('id','image','branch_id','created_at')
                                        ->where('branch_id' , $branch_id)->get();
                
                if(count($get_place_images)>0){
                    $message['data'] = $get_place_images;
                    $message['error'] = 0;
                    $message['message'] = "this is all the images of that place";
                }else{
                    $message['data'] = $get_place_images;
                    $message['error'] = 1;
                    $message['message'] = "there is no images for that place, please trya again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
            
        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function delete_imageBranch(Request $request){
        try{
            $token = $request->input('token');
            $image_id = $request->input('image_id');
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            if($request->has('token') && $check_token != NULL){
                
                $delete_image = \App\Branch_images::where('id',$image_id)->delete();
                
                if( $delete_image == true ){
                    $message['error'] = 0;
                    $message['message'] = "this image is deleted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
            
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function insert_image_branch(Request $request){
        try{
            $token = $request->input('token');
            
            $branch_id = $request->input('branch_id');
            $image = $request->file('image');
            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            
            if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/places';
                $image->move($destinationPath_id, $savedFileName);

                $images = $savedFileName;
            }else{
                $images = "bonbel.jpeg";
            }
            
            
            $check_token = \App\User::where('token',$token)->value('id');
            
            
            if($request->has('token') && $check_token != NULL){
                
                $new_image = new \App\Branch_images;
                $new_image->image = $images;
                $new_image->branch_id = $branch_id;
                $new_image->created_at = $dateTime;
                $new_image->updated_at = $dateTime;
                $new_image->save();
                
                if( $new_image == true){
                    $message['error'] = 0;
                    $message['message'] = " A new image is inserted successfully";
                }else{
                    $message['error'] = 1;
                    $message['message'] = "there is an error, please try again";
                }
                
                
            }else{
                $message['error'] = 2;
                $message['message'] = "there is no user token, please try again";
            }
        }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
   

      
    public function show_places_cityID(Request $request){
        try{

            $token = $request->input('token');
              
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

                        
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
              
                $city_id = $request->input('city_id');

                $get_data = \App\Branch::select('places.id','places.name as place_name','places.logo')
                                        ->JOIN( 'places' , 'branches.place_id' ,'=', 'places.id')
                                        ->where('branches.city_id', $city_id)->get();


                if( count($get_data) >0 ){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is all the places in this city";
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = "there is no places in this city";
                }

            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }


        } catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
    
    public function show_branch_taxs(Request $request){
        
        try{

            $token = $request->input('token');
              
                       
            $check_token = \App\User::where('token',$token)->value('id');
                    
            if( $request->has('token') && $check_token != NULL){
            
                $branch_id = $request->input('branch_id');
                
                $get_data = \App\Branch::select('places.taxs', 'places.service')
                                    ->join('places' , 'branches.place_id' ,'=' ,'places.id')
                                    ->where('branches.id' , $branch_id)->first();
            
                if( $get_data != NULL){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = "this is the taxs and services of that bramch";
                }else{
                    $message['data'] = $get_data;
                    $messgae['error'] = 1;
                    $message['message'] = "No taxs or service";
                }
                
            }else{
                $message['error'] = 3;
                $message['message'] = "there is no user token, please try again";
            }
                
            }catch(Exception $ex){
          $message['error']=2;
          $message['message']="error('DataBase Error: {$ex->getMessage()}')";
        }
      return response()->json($message);
    }
    
  
}
