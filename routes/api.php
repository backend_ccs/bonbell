<?php

use Illuminate\Http\Request;
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

header('Content-Type: application/json; charset=UTF-8', true);
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//logout ::

//block user::

Route::get("block_user","userController@block_user");

Route::get("unblock_user","userController@unblock_user");

Route::get("show_users_block","userController@show_users_block");



Route::get("logout_users", "userController@logout_users");
// big dash ::

Route::post("login","userController@login");

Route::get("show_user_ByID","userController@show_user_ByID");

Route::post("update_adminProfile","userController@update_adminProfile");

Route::get("show_all_users","userController@show_all_users");

Route::get("delete_user","userController@delete_user");

Route::post("add_bonbel_admin","userController@add_bonbel_admin");

Route::get("show_all_bonbeAdmins","userController@show_all_bonbeAdmins");

Route::get("show_branch_taxs","placeController@show_branch_taxs");



// Resturant users ::

Route::post("add_resturant_user","userController@add_resturant_user");   // from main dash

Route::get("show_Resturant_usersAccept","userController@show_Resturant_usersAccept");

Route::get("show_Resturant_usersWaitting","userController@show_Resturant_usersWaitting");

Route::post("add_resturant_admin","userController@add_resturant_admin");   //from resturant dash

Route::get("accept_users","userController@accept_users");   //from main dash to accept resturant admin

Route::post("login_resturant_dash","userController@login_resturant_dash");   


// places ::

Route::get("show_places","placeController@show_places");

Route::get("show_place_ByID","placeController@show_place_ByID");

Route::post("add_place","placeController@add_place");

Route::post("update_place","placeController@update_place");

Route::get("delete_places","placeController@delete_places");


// place description ::


Route::get("show_placeDescription","placeController@show_placeDescription");

Route::get("show_description_ByID","placeController@show_description_ByID");

Route::post("add_description","placeController@add_description");

Route::post("update_description","placeController@update_description");

Route::get("delete_description","placeController@delete_description");

Route::get("show_places_cityID","placeController@show_places_cityID");

// branchess ::

Route::get("show_branches_PlaceID","branchsController@show_branches_PlaceID");

Route::post("add_branches","branchsController@add_branches");

Route::get("delete_branch","branchsController@delete_branch");

Route::get("show_branch_ByID","branchsController@show_branch_ByID");

Route::post("update_branch","branchsController@update_branch");

Route::get("show_branches_AreaID","branchsController@show_branches_AreaID");




//branch images ::

Route::get("show_branch_images","placeController@show_branch_images");

Route::get("delete_imageBranch","placeController@delete_imageBranch");

Route::post("insert_image_branch","placeController@insert_image_branch");



//branch phones ::

Route::get("show_phone_branchId","branchDetailsController@show_phone_branchId");

Route::post("add_branch_phone","branchDetailsController@add_branch_phone");

Route::get("show_phone_ById","branchDetailsController@show_phone_ById");

Route::post("update_phone_branch","branchDetailsController@update_phone_branch");

Route::get("delete_phone","branchDetailsController@delete_phone");

//order types ::

Route::get("show_order_types","branchDetailsController@show_order_types");



// branches type ::

Route::get("show_type_branchId","branchDetailsController@show_type_branchId");

Route::post("add_branch_type","branchDetailsController@add_branch_type");

Route::get("delete_branch_type","branchDetailsController@delete_branch_type");



// about us ::

Route::get("show_aboutUs","aboutController@show_aboutUs");

Route::post("update_aboutUs","aboutController@update_aboutUs");


//terms and condition ::


Route::get("show_termsCondition","aboutController@show_termsCondition");

Route::post("update_termsCondition","aboutController@update_termsCondition");

// city ::

Route::get("show_allCity","cityController@show_allCity");

Route::post("add_city","cityController@add_city");

Route::get("show_city_ById","cityController@show_city_ById");

Route::post("update_city","cityController@update_city");

Route::get("delete_city","cityController@delete_city");


//area data::


Route::get("show_allArea_cityId","cityController@show_allArea_cityId");

Route::post("add_area","cityController@add_area");

Route::get("show_area_ById","cityController@show_area_ById");

Route::post("update_Area","cityController@update_Area");

Route::get("delete_area","cityController@delete_area");

// sauce ::

Route::get("show_sauce_branchID","sauceController@show_sauce_branchID");

Route::post("add_sauce_branchID","sauceController@add_sauce_branchID");

Route::get("delete_sauce","sauceController@delete_sauce");

Route::get("show_sauce_ById","sauceController@show_sauce_ById");

Route::post("update_sauce","sauceController@update_sauce");



// sizes ::

Route::get("show_sizes_itemID","sizesController@show_sizes_itemID");

Route::post("add_sizes_itemID","sizesController@add_sizes_itemID");

Route::get("delete_sizes","sizesController@delete_sizes");

Route::get("show_size_ById","sizesController@show_size_ById");

Route::post("update_sizes","sizesController@update_sizes");



// soft drinks ::

Route::get("show_sofyDrinks_branchID","drinksController@show_sofyDrinks_branchID");

Route::post("add_softDrinks_branchID","drinksController@add_softDrinks_branchID");

Route::get("delete_softDrink","drinksController@delete_softDrink");

Route::get("show_softDrink_ById","drinksController@show_softDrink_ById");

Route::post("update_softDrinks","drinksController@update_softDrinks");


// menu type ::

Route::get("show_menuType_branchID","menuTypeController@show_menuType_branchID");

Route::post("add_menuType_branchID","menuTypeController@add_menuType_branchID");

Route::get("delete_menuType","menuTypeController@delete_menuType");

Route::get("show_menuType_ById","menuTypeController@show_menuType_ById");

Route::post("update_menuType","menuTypeController@update_menuType");



// category type ::

Route::get("show_menuCat_branchID","menuCatController@show_menuCat_branchID");

Route::get("show_menuCat_typeID","menuCatController@show_menuCat_typeID");

Route::post("add_menuCat_typeID","menuCatController@add_menuCat_typeID");

Route::get("delete_menuCat","menuCatController@delete_menuCat");

Route::get("show_menuCat_ById","menuCatController@show_menuCat_ById");

Route::post("update_menuCat","menuCatController@update_menuCat");


//Items ::


Route::get("show_items_menuCatId","itemController@show_items_menuCatId");

Route::post("add_items","itemController@add_items");

Route::get("item_avaliable","itemController@item_avaliable");

Route::get("delete_item","itemController@delete_item");

Route::get("show_items_ById","itemController@show_items_ById");

Route::post("update_item","itemController@update_item");

Route::get("update_sorting_item","itemController@update_sorting_item");

// recomended::

Route::get("show_recommended","itemController@show_recommended");


//features ::


Route::get("show_bonbel_features","featureController@show_bonbel_features");

Route::get("show_vendor_features","featureController@show_vendor_features");

Route::post("add_user_feature","featureController@add_user_feature");

Route::get("show_features_userID","featureController@show_features_userID");

Route::get("delete_userFeature","featureController@delete_userFeature");









//*****************************mobile  apis *****************************************************************************//

Route::post("app_login","userController@app_login");

Route::get("show_city_area","cityController@show_city_area");

Route::get("show_allbranches_or_AreaID","branchsController@show_allbranches_or_AreaID");

Route::get("show_items_branchID","itemController@show_items_branchID");

Route::get("show_allmenu_page","menuTypeController@show_allmenu_page");

//orders ::


//common apis:
Route::post("make_order","orderController@make_order");

Route::get("show_orderNeed_userID","orderController@show_orderNeed_userID");

Route::get("cancel_item","orderController@cancel_item");

Route::get("pay_bill","orderController@pay_bill");



//Dine in Apis::

Route::post("confirm_order","orderController@confirm_order");

Route::get("show_order_time","orderController@show_order_time");

Route::get("show_bill_details","orderController@show_bill_details");

Route::get("split_item","orderController@split_item");

Route::get("show_billSpliting_details","orderController@show_billSpliting_details");


//Fast  && pick  Apis ::

Route::post("confirm_order_fast_pick","orderController@confirm_order_fast_pick");

Route::get("show_billDetails_fast_pick","orderController@show_billDetails_fast_pick");


// reservation ::

Route::post("add_reservation","orderController@add_reservation");

Route::get("show_hall_type","branchsController@show_hall_type");

Route::get("show_reservation_confirm","orderController@show_reservation_confirm");

Route::post("pay_deposit","orderController@pay_deposit");

Route::post("pay_reservation_deposit","orderController@pay_reservation_deposit");

Route::get("get_place_minPresent","orderController@get_place_minPresent");


//orders  in dash ::

Route::get("show_NeedBills_branchID","orderController@show_NeedBills_branchID");

Route::get("show_AcceptBills_branchID","orderController@show_AcceptBills_branchID");

Route::get("show_payedBills_branchID","orderController@show_payedBills_branchID");


Route::get("show_orders_billID","orderController@show_orders_billID");



Route::get("show_code_data","featureController@show_code_data");

Route::get("accept_orders","orderController@accept_orders");



// item page :: 

Route::post("add_item_categories","chooseDetailsController@add_item_categories");
Route::get("show_choose_category","chooseDetailsController@show_choose_category");

Route::post("add_details_categories","chooseDetailsController@add_details_categories");
Route::get("show_details_category","chooseDetailsController@show_details_category");

Route::get("show_item_page","chooseDetailsController@show_item_page");






































